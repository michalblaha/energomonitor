// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS2
{
	[Register ("DailyViewController")]
	partial class DailyViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lbCurrDayPower { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lbCurrDayPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lbPrevDayPower { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lbPrevDayPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lbStreamName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lbCurrDayPower != null) {
				lbCurrDayPower.Dispose ();
				lbCurrDayPower = null;
			}

			if (lbCurrDayPrice != null) {
				lbCurrDayPrice.Dispose ();
				lbCurrDayPrice = null;
			}

			if (lbPrevDayPower != null) {
				lbPrevDayPower.Dispose ();
				lbPrevDayPower = null;
			}

			if (lbPrevDayPrice != null) {
				lbPrevDayPrice.Dispose ();
				lbPrevDayPrice = null;
			}

			if (lbStreamName != null) {
				lbStreamName.Dispose ();
				lbStreamName = null;
			}
		}
	}
}
