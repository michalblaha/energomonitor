// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS2
{
	[Register ("LoginFormController")]
	partial class LoginFormController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnLoginSave { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel txtErrorMsg { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtUsername { get; set; }

		[Action ("CancelClicked:")]
		partial void CancelClicked (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (txtErrorMsg != null) {
				txtErrorMsg.Dispose ();
				txtErrorMsg = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (txtUsername != null) {
				txtUsername.Dispose ();
				txtUsername = null;
			}

			if (btnLoginSave != null) {
				btnLoginSave.Dispose ();
				btnLoginSave = null;
			}
		}
	}
}
