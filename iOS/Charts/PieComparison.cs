﻿using System;
using MonoTouch.UIKit;
using CorePlot;
using Energomonitor.Lib.iOS.Api.JSON;
using System.Linq;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;

namespace iOS2.Charts
{
	public class PieComparison : UIView
	{
		// A simple data source for the plot
		class MyDataSource : CPTPieChartDataSource
		{
			List<double> values = new List<double>(2);
			public MyDataSource(double diffInPercent)
			{
				//calculate percent
				//move it from interval -100-100 into 0-200 interval
				values.Add(diffInPercent+100);
				values.Add(200-(diffInPercent+100));
			}

			public override int NumberOfRecordsForPlot (CPTPlot plot)
			{
				return values.Count;
			}

			public override NSNumber NumberForPlot (CPTPlot plot, CPTPlotField field, uint index)
			{
				return values [(int)index];
			}

			public override CPTFill GetSliceFill (CPTPieChart pieChart, uint recordIndex)
			{
				CPTFill fill;
				if (recordIndex == 0) {
					fill = new CPTFill(CPTColor.OrangeColor);
				} else if (recordIndex == 1) {
					fill = new CPTFill(CPTColor.GreenColor);		
				}
				else
				{
					fill = new CPTFill(CPTColor.BlueColor);
				}
				return fill;
			}
		}



		public System.Drawing.RectangleF RectangleFrame {get; private set;} 
		public IntervalEnum Interval  {get; private set;} 
		public CPTXYGraph graph { get; private set;}
		public double DiffInPercent { get; private set;}

		private MyDataSource data {get; set;} 

		public PieComparison (System.Drawing.RectangleF container, double diffInPercent)
		{
			this.RectangleFrame = container;
			this.Interval = interval;

			this.data = new MyDataSource (diffInPercent);
			DiffInPercent = diffInPercent;
			RenderChart ();
		}

		private CPTXYGraph RenderChart()
		{
			CPTPieChart plot = null;

			var linear = CPTScaleType.Linear;
			graph = new CPTXYGraph (RectangleFrame, linear, linear) {
				Title = "",
				BackgroundColor = new CGColor(0,0,0),
			};
			graph.AxisSet.Hidden = true;
			graph.PaddingLeft = 0.0f;
			graph.PaddingTop = 0.0f;
			graph.PaddingRight = 0.0f;
			graph.PaddingBottom = 0.0f;

			plot = new CPTPieChart { DataSource = data };
			//plot.Frame = new System.Drawing.RectangleF(plot.Frame.X, plot.Frame.Y-150, plot.Frame.Width, plot.Frame.Height);
			plot.CenterAnchor = new System.Drawing.PointF (0.5f, 0.07f);

			float outerRadius = (RectangleFrame.Size.Height /2) * 1.8f;
			float innerRadius = outerRadius * 0.6f;

			//angles in radians : http://coolmath.com/precalculus-review-calculus-intro/precalculus-trigonometry/images/28-trigonometry-01.gif
			plot.StartAngle = (5f/6f)*(float)Math.PI;
			plot.EndAngle = -(1f/2f)*(float)Math.PI;

			plot.PieRadius = outerRadius;
			plot.PieInnerRadius = innerRadius;

			plot.SliceDirection = CPTPieDirection.Clockwise;
			graph.AddPlot (plot);
			//graph.DefaultPlotSpace.Scale (0.1f, PointF.Empty);


			//move it, doesn't work



			return graph;
		}
		public UIView RenderView()
		{


			return new CPTGraphHostingView (RectangleFrame) {
				HostedGraph = graph
			};

		}

	}
}

