﻿using System;
using CorePlot;
using System;
using MonoTouch.UIKit;
using CorePlot;
using Energomonitor.Lib.iOS.Api.JSON;
using System.Linq;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;


namespace iOS2.Charts
{
	public class BarLineComparison
	{
		class MyDataSource : CPTPieChartDataSource
		{
			List<double> current = null;
			List<double> previous = null;
			public MyDataSource(IEnumerable<double> current, IEnumerable<double> previous)
			{
				this.current = current.ToList();
				this.previous = previous.ToList();

			}

			public override int NumberOfRecordsForPlot (CPTPlot plot)
			{
				return values.Count;
			}

			public override NSNumber NumberForPlot (CPTPlot plot, CPTPlotField field, uint index)
			{
				return values [(int)index];
			}

			public override CPTFill GetSliceFill (CPTPieChart pieChart, uint recordIndex)
			{
				CPTFill fill;
				if (recordIndex == 0) {
					fill = new CPTFill(CPTColor.OrangeColor);
				} else if (recordIndex == 1) {
					fill = new CPTFill(CPTColor.GreenColor);		
				}
				else
				{
					fill = new CPTFill(CPTColor.BlueColor);
				}
				return fill;
			}
		}


		public BarLineComparison ()
		{
		}
	}
}

