﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS2
{
	public class Application
	{
	
		public static Energomonitor.Lib.iOS.ApplicationStatus AppStatus = null;	

		// This is the main entry point of the application.
		static void Main (string[] args)
		{
			// if you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here.
			UIApplication.Main (args, null, "AppDelegate");
		}

		public static void EnableTabs(UITabBarController tabController)
		{

			if (Application.AppStatus.LoginStatus != Energomonitor.Lib.iOS.ApplicationStatus.CredentialStatus.Valid) {
				tabController.ViewControllers [0].TabBarItem.Enabled = false;
				tabController.ViewControllers [1].TabBarItem.Enabled = false;
				tabController.ViewControllers [2].TabBarItem.Enabled = false;
				tabController.SelectedIndex = 3;
			} else {
				tabController.ViewControllers [0].TabBarItem.Enabled = true;
				tabController.ViewControllers [1].TabBarItem.Enabled = true;
				tabController.ViewControllers [2].TabBarItem.Enabled = true;			
			}

		}

	}
}
