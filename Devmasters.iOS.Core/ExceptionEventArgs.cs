﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core
{
    public class ExceptionEventArgs : EventArgs
    {
        public ExceptionEventArgs(Exception exc)
            : base()
        {
            Exception = exc;
        }
        public Exception Exception { get; set; }
    }
}
