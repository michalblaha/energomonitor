﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core.Net
{
    public class RequestStartedEventArgs : EventArgs
    {
        public RequestStartedEventArgs(string[] urls)
                  : base()
        {
            this.Urls = urls;
        }
        public string[] Urls {get; private set;}
    }

    public class RequestCompletedEventArgs : EventArgs
    {
        public RequestCompletedEventArgs(Uri url, long length)
            : base()
        {
            this.Url = url;
            this.Length = length;
        }
        public Uri Url { get; private set; }
        public long Length { get; private set; }
    }

}
