﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;

namespace Devmasters.Core.Net
{
    public class WebListener
    {

        public event EventHandler<RequestStartedEventArgs> ServerStarted;
        public event EventHandler ServerEnded;

        public event EventHandler HeartBeat;
        public event EventHandler<RequestStartedEventArgs> RequestStarted;
        public event EventHandler<RequestCompletedEventArgs> RequestCompleted;
        public event UnhandledExceptionEventHandler Error;

        HttpListener listener = new HttpListener();
        List<string> listenerURLs = new List<string>();
        IRequestWorker worker = null;
        bool aborting = false;

        public WebListener(string listeningURL, IRequestWorker worker)
        {
            listenerURLs.Add(listeningURL);
            this.worker = worker;
        }

        public void Start()
        {
            aborting = false;
            worker.RequestStarted += new EventHandler<RequestStartedEventArgs>(thr_RequestStarted);
            worker.RequestCompleted += new EventHandler<RequestCompletedEventArgs>(thr_RequestCompleted);
            worker.Error += new UnhandledExceptionEventHandler(thr_Error);

            foreach (string url in listenerURLs)
                listener.Prefixes.Add(url);

            listener.Start();
            if (ServerStarted != null)
                ServerStarted(this, new RequestStartedEventArgs(listenerURLs.ToArray<string>()));

            for (; ; )
            {
                try
                {
                    HttpListenerContext ctx = listener.GetContext();
                    
                    Thread t = new Thread(worker.ProcessRequest);
                    t.Start(ctx);
                    if (aborting)
                    {
                        t.Join(45000);
                        goto end;
                    }
                }
                catch (ThreadAbortException)
                {
                    goto end;
                }
                catch (Exception e)
                {
                    if (Error != null)
                        Error(this, new UnhandledExceptionEventArgs(e, false));
                }

                finally
                {
                }


            }

            end:
            CleanOnEnd();

        }
        public void Stop()
        {
            aborting = true;
            CleanOnEnd();
        }
        internal void CleanOnEnd()
        {

            listener.Stop();
            listener.Close();
            if (ServerEnded != null)
                ServerEnded(this, EventArgs.Empty);

            worker.RequestStarted -= new EventHandler<RequestStartedEventArgs>(thr_RequestStarted);
            worker.RequestCompleted -= new EventHandler<RequestCompletedEventArgs>(thr_RequestCompleted);
            worker.Error -= new UnhandledExceptionEventHandler(thr_Error);
        }


        void thr_Error(object sender, UnhandledExceptionEventArgs e)
        {
            if (Error != null)
                Error(this.Error, e);
        }

        void thr_RequestCompleted(object sender, RequestCompletedEventArgs e)
        {
            if (RequestCompleted != null)
                RequestCompleted(this, e);
        }

        void thr_RequestStarted(object sender, RequestStartedEventArgs e)
        {
            if (RequestStarted != null)
                RequestStarted(this, e);
        }
    }
}
