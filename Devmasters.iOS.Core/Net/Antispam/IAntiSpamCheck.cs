﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core.Net.Antispam
{

    interface IAntiSpamCheck
    {
        bool IsSpam(string content, string email, string username, string ipaddress);
        int SpamScore(string content, string email, string username, string ipaddress);
        void MarkAsSpam(string content, string email, string username, string ipaddress);
        void MarkAsHam(string content, string email, string username, string ipaddress);
    }
}
