﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core.Net.Antispam
{
    public class StopForumSpam : Base
    {
        public string APIKey { get; private set; }
        public StopForumSpam(string apikey)
            : this(apikey, Base.DEFAULT_THRESHOLD)
        {
        }
        public StopForumSpam(string apikey, int threshold)
            : base(threshold)
        {
            this.APIKey = apikey;
        }

        public override bool IsSpam(string content, string email, string username, string ipaddress)
        {
            return SpamScore(content, email, username, ipaddress) > this.Threshold;
        }

        /*
         http://www.stopforumspam.com/api?ip=91.186.18.61&email=g2fsehis5e@mail.ru&username=MariFoogwoogy
Multi field queries will return data in the following default XML format

<response success="true">
    <type>ip</type>
    <appears>yes</appears>
    <lastseen>2009-10-26 11:55:07</lastseen>
    <frequency>78</frequency>
    <type>email</type>
    <appears>yes</appears>
    <lastseen>2009-06-25 00:24:29</lastseen>
    <frequency>2</frequency>
</response>
         */
        public override int SpamScore(string content, string email, string username, string ipaddress)
        {
            int score = 0;
            string url = "http://www.stopforumspam.com/api?ip={2}&email={1}&username={0}&f=xmlcdata";
            Web.URLContent req = new Web.URLContent(string.Format(url, username, email, ipaddress));
            string ret = req.GetContent("utf-8");
            if (!string.IsNullOrEmpty(ret) && ret[0] == 'Y')
            {
                string[] parts = ret.Split(new char[] { '|' }, StringSplitOptions.None);
                if (parts[0].ToLower() == "n")
                    return 0;
                else
                {
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[3]))
                        if (Convert.ToInt32(parts[3]) > 0)
                            score += 30;
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[5]))
                        if (Convert.ToInt32(parts[5]) > 0)
                            score += 30;
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[7]))
                        if (Convert.ToInt32(parts[7]) > 0)
                            score += 30;

                    return score;
                }

            }
            else
                return 0;

        }
        public override void MarkAsSpam(string content, string email, string username, string ipaddress)
        {
            //throw new NotImplementedException();

        }

        public override void MarkAsHam(string content, string email, string username, string ipaddress)
        {
            throw new NotImplementedException();
        }
    }
}
