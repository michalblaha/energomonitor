﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core.Net.Antispam
{
    public class BotScout : Base
    {
        public string APIKey { get; private set; }
        public BotScout(string apikey)
            : this(apikey, Base.DEFAULT_THRESHOLD)
        {
        }
        public BotScout(string apikey, int threshold)
            : base(threshold)
        {
            this.APIKey = apikey;
        }

        public override bool IsSpam(string content, string email, string username, string ipaddress)
        {
            return SpamScore(content, email, username, ipaddress) > this.Threshold;
        }

        //http://botscout.com/test/?multi&name=krasnhello&mail=krasnhello@mail.ru&ip=84.16.230.111
        //Y|MULTI|IP|7|MAIL|2|NAME|7
        public override int SpamScore(string content, string email, string username, string ipaddress)
        {
            int score = 0;
            string url = "http://botscout.com/test/?multi&name={0}&mail={1}&ip={2}";
            Web.URLContent req = new Web.URLContent(string.Format(url, username, email, ipaddress));
            string ret = req.GetContent("utf-8");
            if (!string.IsNullOrEmpty(ret) && ret[0] == 'Y')
            {
                string[] parts = ret.Split(new char[] { '|' }, StringSplitOptions.None);
                if (parts[0].ToLower() == "n")
                    return 0;
                else
                {
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[3]))
                        if (Convert.ToInt32(parts[3]) > 0)
                            score += 30;
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[5]))
                        if (Convert.ToInt32(parts[5]) > 0)
                            score += 30;
                    if (Devmasters.Core.TextUtil.IsNumeric(parts[7]))
                        if (Convert.ToInt32(parts[7]) > 0)
                            score += 30;

                    return score;
                }

            }
            else
                return 0;

        }
        public override void MarkAsSpam(string content, string email, string username, string ipaddress)
        {
            //throw new NotImplementedException();

        }

        public override void MarkAsHam(string content, string email, string username, string ipaddress)
        {
            throw new NotImplementedException();
        }
    }
}
