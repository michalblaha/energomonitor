﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core.Net.Antispam
{
    public abstract class Base : IAntiSpamCheck
    {
        public static int DEFAULT_THRESHOLD = 60;
        public int Threshold { get; protected set; }
        public Base()
            : this(DEFAULT_THRESHOLD)
        { 
        }        
        public Base(int threshold)
        {
            this.Threshold = threshold;
        }


        public abstract bool IsSpam(string content, string email, string username, string ipaddress);
        public abstract int SpamScore(string content, string email, string username, string ipaddress);
        public abstract void MarkAsSpam(string content, string email, string username, string ipaddress);
        public abstract void MarkAsHam(string content, string email, string username, string ipaddress);


    }
}
