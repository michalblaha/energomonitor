﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Devmasters.Core.Net
{
    public interface IRequestWorker
    {
        event EventHandler<RequestStartedEventArgs> RequestStarted;
        event EventHandler<RequestCompletedEventArgs> RequestCompleted;
        event UnhandledExceptionEventHandler Error;

        void ProcessRequest(object httpListenerContext);
    }
}
