using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using System.ComponentModel;

namespace Devmasters.Core
{



    public static class EnumExtension
    {
        public static string FullEnumValue(this Enum value)
        {
            return String.Format("{0}_{1}", value.GetType().Name.ToString(), Enum.GetName(value.GetType(), value));

        }

        public static string ToNiceDisplayName(this Enum value)
        {
            string result = value.ToString();
            //try NiceDisplayName
            if (value.GetType().GetCustomAttributes(false).Length > 0)
            {
                foreach (object o in value.GetType().GetCustomAttributes(false))
                {
                    if (o.GetType() == typeof(ShowNiceDisplayNameAttribute))
                    {
                        //look for NiceDisplayAttribute
                        object[] attributes = value.GetType().GetField(result).GetCustomAttributes(false);
                        if (attributes.Length > 0)
                        {
                            foreach (object oa in attributes)
                            {
                                if (oa.GetType() == typeof(NiceDisplayNameAttribute))
                                {
                                    NiceDisplayNameAttribute dnn = (NiceDisplayNameAttribute)oa;
                                    return dnn.DisplayName;
                                }
                            }
                        }


                    }
                }
            }
            return result;

        }

        public static string ToLocalizedString(this Enum value)
        {
            LocalizableEnumAttribute locAttr = Enums.GetLocalizableEnum(value.GetType());
            if (locAttr != null && locAttr.ResourceManager != null)
            {

                string result = locAttr.ResourceManager.GetString(value.FullEnumValue(), System.Globalization.CultureInfo.CurrentUICulture);

                if (string.IsNullOrEmpty(result))
                {
                    //default value
                    result = value.ToNiceDisplayName();


                }

                return result;
            }
            else
                return value.ToNiceDisplayName();
        }

        public static bool IsDefault(this Enum value)
        {
            object[] attributes = value.GetType().GetField(value.ToString()).GetCustomAttributes(false);
            foreach (object oa in attributes)
                if (oa.GetType() == typeof(DefaultValueAttribute))
                    return true;

            return false;
        }

    }

    public class Enums
    {
        public struct KeyValue
        {
            public string Key;
            public string Value;
            public bool Default;
            internal int? Sortvalue;
        }

        public static bool IsLocalizable(Type enumType)
        {
            if (enumType.GetCustomAttributes(false).Length > 0)
            {
                foreach (object o in enumType.GetCustomAttributes(false))
                {
                    if (o.GetType() == typeof(LocalizableEnumAttribute))
                    {
                        LocalizableEnumAttribute attr = (LocalizableEnumAttribute)o;
                        return attr.IsLocalizable;
                    }
                }
            }
            return false;

        }

        public static bool IsSortable(Type enumType, out SortableAttribute.SortAlgorithm algorithm)
        {
            algorithm = SortableAttribute.SortAlgorithm.None;
            if (enumType.GetCustomAttributes(false).Length > 0)
            {
                foreach (object o in enumType.GetCustomAttributes(false))
                {
                    if (o.GetType() == typeof(SortableAttribute))
                    {
                        algorithm = ((SortableAttribute)o).Algorithm;
                        return true;
                    }
                }
            }

            return false;
        }

        public static LocalizableEnumAttribute GetLocalizableEnum(Type enumType)
        {
            if (enumType.GetCustomAttributes(false).Length > 0)
            {
                foreach (object o in enumType.GetCustomAttributes(false))
                {
                    if (o.GetType() == typeof(LocalizableEnumAttribute))
                    {
                        return (LocalizableEnumAttribute)o;
                    }
                }
            }
            return null;

        }

        public static bool HasNiceNames(Type enumType)
        {
            if (enumType.GetCustomAttributes(false).Length > 0)
            {
                foreach (object o in enumType.GetCustomAttributes(false))
                {
                    if (o.GetType() == typeof(ShowNiceDisplayNameAttribute))
                        return true;
                }
            }
            return false;
        }

        public static int? GetMaxValue(Type enumType)
        {
            if (!enumType.IsEnum)
                throw new InvalidCastException("Parameters must be enumeration");

            foreach (string name in Enum.GetNames(enumType))
            {
                FieldInfo f = enumType.GetField(name);
                if (f != null)
                {
                    object[] attributes = f.GetCustomAttributes(false);
                    if (attributes.Length > 0)
                    {
                        foreach (object o in attributes)
                        {
                            if (o.GetType() == typeof(ThisIsMaxValueAttribute))
                            {
                                ThisIsMaxValueAttribute attr = o as ThisIsMaxValueAttribute;
                                if (attr != null)
                                {
                                    if (attr.MaxValue == int.MinValue)
                                    {
                                        try
                                        {
                                            return Convert.ToInt32(Enum.Parse(enumType, name));
                                        }
                                        catch
                                        {
                                            return null;
                                        }
                                    }

                                    else
                                        return attr.MaxValue;
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }


        public static System.Enum GetValueOrDefaultValue(string inputValue, Type enumType)
        {
            if (!enumType.IsEnum)
                throw new InvalidCastException("Parameters must be enumeration");

            if (!string.IsNullOrEmpty(inputValue))
                if (Enum.IsDefined(enumType, inputValue))
                    //return Enum.Parse(enumType, inputValue, true);
                    return (System.Enum)Enum.Parse(enumType, inputValue, true);



            //return defaultValue
            foreach (string name in Enum.GetNames(enumType))
            {
                FieldInfo f = enumType.GetField(name);
                if (f != null)
                {
                    object[] attributes = f.GetCustomAttributes(false);
                    if (attributes.Length > 0)
                    {
                        foreach (object o in attributes)
                        {
                            if (o.GetType() == typeof(DefaultValueAttribute))
                            {
                                return (System.Enum)Enum.Parse(enumType, name);
                            }
                        }
                    }
                }
            }
            throw new ArgumentOutOfRangeException("inputValue doesn't exists in this enumeration and no default value is set");
        }



        public static bool IsDisabled(FieldInfo field)
        {
            if (field != null)
            {
                object[] attributes = field.GetCustomAttributes(false);
                if (attributes.Length > 0)
                {
                    foreach (object o in attributes)
                    {
                        if (o.GetType() == typeof(DisabledAttribute))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static string GetNiceDisplayName(FieldInfo field)
        {
            if (field != null)
            {
                object[] attributes = field.GetCustomAttributes(false);
                if (attributes.Length > 0)
                {
                    foreach (object o in attributes)
                    {
                        if (o.GetType() == typeof(NiceDisplayNameAttribute))
                        {
                            NiceDisplayNameAttribute dnn = (NiceDisplayNameAttribute)o;
                            return dnn.DisplayName;
                        }
                    }
                }
            }
            return null;
        }

        public static int? GetSortableValue(FieldInfo field)
        {
            if (field != null)
            {
                object[] attributes = field.GetCustomAttributes(false);
                if (attributes.Length > 0)
                {
                    foreach (object o in attributes)
                    {
                        if (o.GetType() == typeof(SortValueAttribute))
                        {
                            SortValueAttribute dnn = (SortValueAttribute)o;
                            return dnn.SortValue;
                        }
                    }
                }
            }
            return null;
        }

        public static List<KeyValue> EnumToEnumerable(Type enumType)
		{
			if (!enumType.IsEnum)
				throw new InvalidCastException("Parameters must be enumeration");

			bool useNiceNames = HasNiceNames(enumType);
			bool localizable = IsLocalizable(enumType); ;
        
            SortableAttribute.SortAlgorithm sortAlg;
            bool sortable = IsSortable(enumType, out sortAlg);

			List<KeyValue> coll = new List<KeyValue>();

			foreach (string name in Enum.GetNames(enumType))
			{
				//is disabled?
				if (IsDisabled(enumType.GetField(name)))
					continue; //is disabled, skip this value

		
				KeyValue kv = default(KeyValue);
				kv.Key = null;
				kv.Default = ((Enum)Enum.Parse(enumType, name)).IsDefault();

				if (localizable)
				{
					string locKey = ((Enum)Enum.Parse(enumType, name)).ToLocalizedString();
					if (!string.IsNullOrEmpty(locKey))
					    kv.Key = locKey;

				}

				if (useNiceNames && kv.Key == null)
				{
					kv.Key = GetNiceDisplayName(enumType.GetField(name));
				}

				//default. value for all other cases
				if (kv.Key == null)
					kv.Key = name;

				
				if (Enum.GetUnderlyingType(enumType).UnderlyingSystemType == typeof(byte))
					 kv.Value = ((int)Enum.Parse(enumType, name)).ToString();
				else if (Enum.GetUnderlyingType(enumType).UnderlyingSystemType == typeof(Int16))
					 kv.Value = ((int)Enum.Parse(enumType, name)).ToString();
				else if (Enum.GetUnderlyingType(enumType).UnderlyingSystemType == typeof(Int64))
					 kv.Value = Convert.ToInt32((string)Enum.Parse(enumType, name)).ToString();
				else
					 kv.Value = ((Int32)Enum.Parse(enumType, name)).ToString();
				
				
                //kv.Value = Enum.Parse(enumType, name).ToString();

                if (sortable)
                {
                    kv.Sortvalue = GetSortableValue(enumType.GetField(name)) ;
                }

				coll.Add(kv);
			}
            if (sortable)
            {

                coll = Sort(coll, sortAlg).ToList();
            }

			return coll;
		}

        private static IEnumerable<KeyValue> Sort(IEnumerable<KeyValue> collection, SortableAttribute.SortAlgorithm algoritm)
        {
            switch (algoritm)
            {
                case SortableAttribute.SortAlgorithm.AlphabeticallyOnly:
                    return collection.OrderBy(m => m.Key);
                    break;
                case SortableAttribute.SortAlgorithm.BySortValueAndThenAlphabetically:
                    return collection.Where(m => m.Sortvalue.HasValue).OrderBy(n => n.Sortvalue.Value)
                            .Concat(collection.Where(m => !m.Sortvalue.HasValue).OrderBy(n => n.Key));
                    break;
                case SortableAttribute.SortAlgorithm.BySortValue:
                    return collection.Select(m => new KeyValue()
                                                        {
                                                            Default = m.Default,
                                                            Key = m.Key,
                                                            Value = m.Value,
                                                            Sortvalue = m.Sortvalue.HasValue ? m.Sortvalue.Value : Convert.ToInt32(m.Value),
                                                        })
                                    .OrderBy(n => n.Sortvalue);

                    ;
                    break;
                default: //SortableAttribute.SortAlgorithm.None
                    return collection;
                    break;
            }
        }


    }

    [AttributeUsage(AttributeTargets.All)]
    public class NiceDisplayNameAttribute : Attribute
    {
        protected string displayedValue = "";
        // The constructor is called when the attribute is set.
        public NiceDisplayNameAttribute(string value)
        {
            displayedValue = value;
        }

        public string DisplayName
        {
            get { return displayedValue; }
        }
    }

    [AttributeUsage(AttributeTargets.Enum)]
    public class LocalizableEnumAttribute : Attribute
    {
        protected bool isLocalizable = false;
        protected ResourceManager2 rm;
        // The constructor is called when the attribute is set.
        public LocalizableEnumAttribute(bool isLocalizable, Type type)
        {
            this.isLocalizable = isLocalizable;

            if (type.GetInterface("IResourceManager2") != null)
            {
                IResourceManager2 irm = System.Activator.CreateInstance(type) as IResourceManager2;
                this.rm = irm.Manager;
            }
        }

        public ResourceManager2 ResourceManager
        {
            get { return rm; }
        }

        public bool IsLocalizable
        {
            get { return isLocalizable; }
        }
    }


    [AttributeUsage(AttributeTargets.Enum)]
    public class ShowNiceDisplayNameAttribute : Attribute
    {
        public ShowNiceDisplayNameAttribute()
        {
        }

    }

    [AttributeUsage(AttributeTargets.Enum)]
    public class SortableAttribute : Attribute
    {
        public enum SortAlgorithm
        {
            AlphabeticallyOnly,
            BySortValueAndThenAlphabetically,
            BySortValue,
            None
        }
        public SortAlgorithm Algorithm { get; private set; }
        public SortableAttribute(SortAlgorithm algorithm)
        {
            this.Algorithm = algorithm;
        }

    }

    [AttributeUsage(AttributeTargets.All)]
    public class SortValueAttribute : Attribute
    {
        public int SortValue { get; private set; }
        public SortValueAttribute(int value)
        {
            SortValue = value;
        }


    }

    [AttributeUsage(AttributeTargets.All)]
    public class DisabledAttribute : Attribute
    {
        public DisabledAttribute()
        {
        }

    }

    [AttributeUsage(AttributeTargets.All)]
    public class ThisIsMaxValueAttribute : Attribute
    {
        protected int value = int.MinValue;
        // The constructor is called when the attribute is set.
        public ThisIsMaxValueAttribute()
        { }

        public ThisIsMaxValueAttribute(int value)
        {
            this.value = value;
        }

        public int MaxValue
        {
            get { return this.value; }
        }
    }



}
