﻿using System;
using System.Globalization;
using System.Linq;
using System.Resources;

namespace Devmasters.Core
{
    internal class Manager
    {
        private ResourceManager2 rmTexts;


        internal Manager()
        {
            rmTexts = new ResourceManager2("Devmasters.Core.Texts", this.GetType().Assembly);
        }

        internal ResourceManager2 TextReader
        {
            get
            {
            	return rmTexts;
            }
        }


    }
}
