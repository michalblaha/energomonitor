﻿using System;
using System.Diagnostics;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Generic;

namespace Devmasters.Core
{
	public static class TextUtil
	{

        private const string RANDOMCHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";

		public static string DEFAULTCOLLECTIONDIVIDER = "|";
        public static string Space = " ";


		static System.Random randomgenerator = new Random();
		private static CultureInfo[] scis = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
		public static CultureInfo USCulture = new System.Globalization.CultureInfo("en-US");
		private static System.Collections.Generic.Dictionary<string, string> HTMLEntities = new System.Collections.Generic.Dictionary<string, string>();
		private static object lockObj = new object();

		static TextUtil()
		{
			FillHTMLEntityTable();
		}

		public static string Reverse(string text)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;

			System.Text.StringBuilder sb = new StringBuilder(text.Length);
			for (int i = text.Length - 1; i >= 0; i--)
			{
				sb.Append(text[i]);
			}
			return sb.ToString();
		}

		public static string NormalizeToURL(string title, string body)
		{
			string done = string.Empty;
			if (title.Trim().Length > 0)
			{
				done = NormalizeToURL(ShortenText(title, 45));
			}
			if (done.Length == 0)
			{
				done = NormalizeToURL(ShortenText(body, 45));
			}

			return done;


		}
		public static string NormalizeToURL(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}
			s = RemoveDiacritics(s).ToLower();
			s = Regex.Replace(s, "[^a-z0-9]", "-");
			s = s.Trim(new char[] { '-' });
			while (s.IndexOf("--") > -1)
			{
				s = s.Replace("--", "-");
			}
			return s;
		}

		public static string ShortenText(string sText, int Len)
		{
			return ShortenText(sText, Len, " ", string.Empty);
		}
		public static string ShortenText(string sText, int Len, string afterShorten)
		{
			return ShortenText(sText, Len, " ", afterShorten);
		}

		public static string ShortenText(string sText, int Len, string Delimiter, string afterShorten)
		{
            if (string.IsNullOrEmpty(sText))
                return string.Empty;
			if (sText.Length > Len)
			{
				string res = sText.Substring(0, Len);
				if (Delimiter.Length > 0)
				{
					if (sText.Substring(Len, 1) == Delimiter)
					{
						res = sText.Substring(0, Len);
					}
					else
					{
						int iTmp = sText.LastIndexOf(Delimiter, Len, StringComparison.InvariantCulture);
						if (iTmp > 0)
						{
							res = sText.Substring(0, iTmp);
						}
					}
				}

				if (afterShorten.Length > 0)
					res += afterShorten;

				return res;
			}
			return sText;
		}


        public static string ReplaceDuplicates(string text, char duplicated)
        { return ReplaceDuplicates(text, duplicated, duplicated); }

        public static string ReplaceDuplicates(string text, char duplicated, char replacement)
        { 
            string regex = string.Format("([{0}]{{2,}})",duplicated);
            return Regex.Replace(text, regex, replacement.ToString()); 
        }

        public static string ReplaceDuplicates(string text, string duplicatedRegexReady)
        { return ReplaceDuplicates(text, duplicatedRegexReady, duplicatedRegexReady); }

        public static string ReplaceDuplicates(string text, string duplicatedRegexReady, string replacement)
        {
            string regex = string.Format("(({0}){{2,}})", duplicatedRegexReady);
            return Regex.Replace(text, regex, replacement);
        }

        public static string NormalizeToPureTextLower(this string s)
        {
            return normalizeToPureTextLower(s);
        }
        private static string normalizeToPureTextLower(string s)
        { 
            return System.Text.RegularExpressions.Regex.Replace(RemoveDiacritics(s).ToLower(), @"[^a-z0-9\- ]", string.Empty);
        }

        //public static string RemoveDiacritics(this string s)
        //{
        //    return removeDiacritics(s);
        //}
        public static string RemoveDiacritics(string s)
        {
            return removeDiacritics(s);
        }

        private static string removeDiacritics(string s)
		{
            if (s == null)
                return null;
            if (s.Length == 0)
                return string.Empty;
			s = s.Normalize(NormalizationForm.FormD);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < s.Length; i++)
			{
				if (CharUnicodeInfo.GetUnicodeCategory(s[i]) != UnicodeCategory.NonSpacingMark)
				{
					sb.Append(s[i]);
				}
			}
			return sb.ToString();
		}

		public static string NormalizeToBlockText(string sText)
		{
			sText = RemoveHTML(sText);

			//remove strange

            sText = ReplaceCRFL(sText, Space);

            sText = sText.Replace("\f", Space);
            sText = sText.Replace("\r", Space);
            sText = sText.Replace("\t", Space);


			return ReplaceDuplicates(sText, ' ').Trim();

		}

		public static string ShortenHTML(string sText, int Len, string[] AllowedTags)
		{
			return ShortenHTML(sText, Len, AllowedTags, " ", "...");
		}


		public static string ShortenHTML(string sText, int Len, string[] AllowedTags, string Delimiter)
		{
			return ShortenHTML(sText, Len, AllowedTags, Delimiter, "...");
		}


		public static string RemoveHTML(string sText)
		{
            if (sText == null)
                return null;
            sText = Regex.Replace(sText,"<br\\s?/?>", " ", RegexOptions.IgnoreCase);
			return ReplaceHTMLEntities(Regex.Replace(sText, "<[^>]*>", string.Empty));
		}


		public static string ShortenHTML(string sText, int Len, string[] AllowedTags, string Delimiter, string AfterShort)
		{
			if (string.IsNullOrEmpty(sText))
				return string.Empty;

			foreach (string s in AllowedTags)
			{
				sText = sText.Replace("<" + s.ToLower() + ">", "|" + s.ToLower() + ">"); //<i> -> |i>
				sText = sText.Replace("<" + s.ToLower() + " ", "|" + s.ToLower() + " "); //<li ...> -> |li ....>
				sText = sText.Replace("</" + s.ToLower() + ">", "|/" + s.ToLower() + ">");

				sText = sText.Replace("<" + s.ToUpper() + ">", "|" + s.ToLower() + ">");
				sText = sText.Replace("<" + s.ToUpper() + " ", "|" + s.ToLower() + " ");
				sText = sText.Replace("</" + s.ToUpper(), "|/" + s.ToLower());
			}
			sText = RemoveHTML(sText);
			foreach (string s in AllowedTags)
			{
				sText = sText.Replace("|" + s.ToLower(), "<" + s);
				sText = sText.Replace("|/" + s.ToLower(), "</" + s);
			}
			if (sText.Length > Len)
			{
				string sShortText = string.Empty;
				bool bFirstTagOpen = false;
				bool bSecondTagOpen = false;
				bool bTagOpen = false;
				int _Vb_t_i4_2 = Len - 1;
				for (int i = 0; i <= _Vb_t_i4_2; i++)
				{
					string sBuff = string.Empty;
					string sChar = sText.Substring(i, 1);
					if ((sChar == "<") & !bFirstTagOpen)
					{
						bFirstTagOpen = true;
						if ((sText.Length > (i + 4)) && (sText.Substring(i, 5).ToLower() == "<img "))
						{
							bFirstTagOpen = false;
							bTagOpen = true;
							bSecondTagOpen = true;
						}
					}
					if ((sChar == "<") & bTagOpen)
					{
						bSecondTagOpen = true;
					}
					if ((sChar == ">") & bFirstTagOpen)
					{
						bFirstTagOpen = false;
						bTagOpen = true;
					}
					if ((sChar == ">") & bTagOpen)
					{
						bFirstTagOpen = false;
						bTagOpen = false;
						bSecondTagOpen = false;
						sShortText = sShortText + sBuff;
						sBuff = string.Empty;
					}
					if (bTagOpen)
					{
						sBuff = sBuff + sChar;
					}
					else
					{
						sShortText = sShortText + sChar;
					}

					Debug.Write(bSecondTagOpen);
				}
				if (Delimiter.Length > 0)
				{
					if (sShortText.EndsWith(Delimiter))
					{
						return (sShortText + AfterShort);
					}
					int iTmp = sShortText.LastIndexOf(Delimiter, StringComparison.InvariantCulture);
					if (iTmp > 0)
					{
						return (sShortText.Substring(0, iTmp) + AfterShort);
					}
				}
				return (sShortText + AfterShort);
			}
			return sText;
		}


		public static byte[] ComputeHash(byte[] data)
		{
			HashAlgorithm algo;
			algo = MD5.Create();
			byte[] hash = algo.ComputeHash(data);
			return data;
		}

		public static byte[] ComputeHash(string text)
		{            
			return ComputeHash(Encoding.UTF8.GetBytes(text));
		}

		public static string ComputeHashString(string text)
		{
            if (string.IsNullOrEmpty(text))
                return string.Empty;
			return Convert.ToBase64String(ComputeHash(text));
		}

		public static string ComputeHashString(byte[] data)
		{
			return Convert.ToBase64String(data);
		}


        /// <summary>
        /// returns random string of specified length usign letters from text
        /// </summary>
        private static string GenRandomString(Random random, string text, int length)
        {
            length = length > 0 ? length : text.Length;

            var builder = new StringBuilder();

            for (int i = 0; i < length; i++)
                builder.Append(text[random.Next(text.Length)]);

            return builder.ToString();
        }

		public static string GenRandomString(int length)
		{
            var random = new Random();

            return GenRandomString(random, RANDOMCHARACTERS, length);
        }

		public static double Rnd()
		{
			return randomgenerator.NextDouble();
		}

		public static bool IsNumeric(string text)
		{
            if (string.IsNullOrEmpty(text))
                return false;
			foreach (char c in text)
            {
                int ci = (int)c;
                if (ci < 48 || ci > 57)
                    return false;
			}
			return true;
		}

		public static bool IsASCIIText(string text)
		{
			foreach (char c in text)
			{
				int ci = (int)c;
				if (ci > 127 || (ci < 32 && !(ci == 9 || ci == 10 || ci == 13)))
					return false;
			}
			return true;
		}
		public static CultureInfo GetNeutralCulture(CultureInfo culture)
		{
			return GetNeutralCulture(culture, CultureInfo.GetCultureInfo("en"));
		}

		public static CultureInfo GetNeutralCulture(CultureInfo culture, CultureInfo defaultCultureInfo)
		{
			if (culture.IsNeutralCulture == false)
				return culture.Parent;
			else
				return culture;

		}

		public static string CollectionToString(System.Collections.ICollection list)
		{
			return CollectionToString(list, DEFAULTCOLLECTIONDIVIDER);
		}
		public static string CollectionToString(System.Collections.ICollection list, string divider)
		{
			if (list == null || list.Count == 0)
				return string.Empty;

			StringBuilder sb = new StringBuilder(128);
			foreach (string item in list)
			{
				sb.Append(item);
				if (divider.Length > 0)
					sb.Append(divider);
			}
			if (divider.Length > 0)
				sb.Remove(sb.Length - divider.Length, divider.Length);

			return sb.ToString();
		}

		public static CultureInfo GetSpecificCulture(CultureInfo culture)
		{
			return GetSpecificCulture(culture, CultureInfo.GetCultureInfo("en-US"));
		}

		public static CultureInfo GetSpecificCulture(CultureInfo culture, CultureInfo defaultCultureInfo)
		{
			if (culture.IsNeutralCulture)
			{
				foreach (CultureInfo ci in scis)
				{
					if (ci.Parent.LCID == culture.LCID)
					{
						return ci;
					}
				}
				return defaultCultureInfo;
			}
			return culture;
		}

        public static List<CultureInfo> GetSpecificCultures(CultureInfo culture)
        {
            List<CultureInfo> cultures = new List<CultureInfo>();
            if (culture.IsNeutralCulture)
            {
                foreach (CultureInfo ci in scis)
                {
                    if (ci.Parent.LCID == culture.LCID)
                    {
                        cultures.Add(ci);
                    }
                }
            }
            return cultures;
        }


		public static bool IsValidEmail(string email)
		{
			return Regex.IsMatch(email, @"^[a-z0-9_\.-]+@[a-z0-9\._-]+\.[a-z]{2,6}$", RegexOptions.IgnoreCase);
		}
	
		public static bool GuidTryParse(string s, out Guid result)
		{
			result = Guid.Empty;
			if (string.IsNullOrEmpty(s))
				return false;

			Regex format = new Regex(
				 "^[A-Fa-f0-9]{32}$|" +
				 "^({|\\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\\))?$|" +
				 "^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$");
			Match match = format.Match(s);
			if (match.Success)
			{
				result = new Guid(s);
				return true;
			}
			else
			{
				result = Guid.Empty;
				return false;
			}
		}

		public static string InsertSpacesBeforeUpperCase(string value)
		{
			const string PATTERN = @"(?!^)([A-Z])(?<! [A-Z])";

			Match match;
			do
			{
				match = Regex.Match(value, PATTERN);
				if (!match.Success)
					break;

				value = value.Replace(match.Value, String.Concat(" ", match.Value));

			} while (match.Success);

			return value;
		}

		public static int? ConvertToInt(string value, int? valueIfNull)
		{
			int val;
			return int.TryParse(value, out val) ? val : valueIfNull;
		}

		public static string EncodeJsString(string value)
		{
			StringBuilder sb = new StringBuilder();

			foreach (char c in value)
			{
				switch (c)
				{
					case '\"':
						sb.Append("\\\"");
						break;

					case '\\':
						sb.Append("\\\\");
						break;

					case '\b':
						sb.Append("\\b");
						break;

					case '\f':
						sb.Append("\\f");
						break;

					case '\n':
						sb.Append("\\n");
						break;

					case '\r':
						sb.Append("\\r");
						break;

					case '\'':
						sb.Append("\\'");
						break;

					case '\t':
						sb.Append("\\t");
						break;

					default:
						int i = (int)c;
						if (i < 32 || i > 127)
						{
							sb.AppendFormat("\\u{0:X04}", i);
						}
						else
						{
							sb.Append(c);
						}
						break;
				}
			}

			return sb.ToString();
		}

		public static string IfEmpty(string value, string nullvalue)
		{
			return (String.IsNullOrEmpty(value)) ? nullvalue : value;
		}

		private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
		public static bool IsGuidValid(string strguid, out Guid guid)
		{
			Guid? guidn = null;
			if (IsGuidValid(strguid, out guidn))
			{
				guid = guidn.Value;
				return true;
			}
			else
			{
				guid = Guid.Empty;
				return false;
			}
		}
		public static bool IsGuidValid(string strguid, out Guid? guid)
		{
			bool isValid = false;
			guid = null;
			if (strguid != null)
			{

				if (isGuid.IsMatch(strguid))
				{
					guid = new Guid(strguid);
					isValid = true;
				}
			}
			return isValid;
		}
		public static string Capitalize(this string instance)
		{
			if (String.IsNullOrEmpty(instance))
				return String.Empty;

			return instance.Substring(0, 1).ToUpper() + instance.Substring(1).ToLower();
		}

		public static string ConvertToAllowHtml(string text)
		{
			string output = RemoveHTML(text);
			output = output.Replace("\r", "");
			output = output.Replace("\n", "<br/>");

			// detect URL
			output = Regex.Replace(output, @"http(s)?://([a-zA-Z0-9-_]+\.){1,2}[a-zA-Z]+(/[a-zA-Z0-9\.\-_]+)*", "<a href=\"$&\">$&</a>");

			return output;
		}

		static string lf = "\n";
		static string cr = "\r";
		static string crlf = cr + lf;
		static string br = @"<br>";
		public static string FormatPlainTextForArticle(string plainText)
		{
			string lf = "\n";
			string cr = "\r";
			string crlf = cr + lf;
			string br = @"<br>";
			string newBody;
			newBody = RemoveHTML(plainText);
			newBody = ReplaceCRFL(newBody, br);
			if (newBody.Length > 5)
			{
				int pos = 0;
				while (newBody.IndexOf(br + br + br, pos) > -1)
				{
					newBody = newBody.Replace(br + br + br, br + br);
				}
			}
			return newBody;
		}

		static Regex findEntityRegex = new Regex("(&[a-zA-Z0-9#]*;)", RegexOptions.Compiled);
		public static string ReplaceHTMLEntities(string text)
		{
			MatchEvaluator myEvaluator = new MatchEvaluator(ReplaceHTMLEntity);
			return findEntityRegex.Replace(text, myEvaluator);
		}

		private static string ReplaceHTMLEntity(Match m)
		{
			if (m.Success)
			{
				if (m.Value.Contains("#"))
				{
					int? ascii = ConvertToInt(m.Value.Substring(2,m.Value.Length-3), 0);
					if (ascii > 31 && ascii<8192)
						return Convert.ToChar(ascii).ToString();
				}
				else
				{
					string val = m.Value;

					if (HTMLEntities.ContainsKey(m.Value))
						return HTMLEntities[m.Value];
					else
					{
						val = val.ToLower();
						if (HTMLEntities.ContainsKey(val))
							return HTMLEntities[val];
					}
				}
			}
			return m.Value;
		}

		public static string ReplaceCRFL(string text, string newValue)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;
			if (newValue == null)
				throw new ArgumentNullException("newValue");

			if (text.Contains(crlf))
			{
				text = text.Replace(crlf, newValue);
			}
			if (text.Contains(cr))
			{
				text = text.Replace(cr, newValue);
			}
			if (text.Contains(lf))
			{
				text = text.Replace(lf, newValue);
			}
			return text;
		}




		private static void FillHTMLEntityTable()
		{
			lock (lockObj)
			{
				HTMLEntities.Clear();

				HTMLEntities.Add("&quot;", "\"");
				HTMLEntities.Add("&amp;", "&");
				HTMLEntities.Add("&apos;", "'");
				//HTMLEntities.Add("&lt;", "<");
				//HTMLEntities.Add("&gt;", ">");
				HTMLEntities.Add("&nbsp;", " ");
				HTMLEntities.Add("&iexcl;", "¡");
				HTMLEntities.Add("&cent;", "¢");
				HTMLEntities.Add("&pound;", "£");
				HTMLEntities.Add("&curren;", "¤");
				HTMLEntities.Add("&yen;", "¥");
				HTMLEntities.Add("&brvbar;", "¦");
				HTMLEntities.Add("&sect;", "§");
				HTMLEntities.Add("&uml;", "¨");
				HTMLEntities.Add("&copy;", "©");
				HTMLEntities.Add("&ordf;", "ª");
				HTMLEntities.Add("&laquo;", "«");
				HTMLEntities.Add("&not;", "¬");
				HTMLEntities.Add("&shy;", Convert.ToChar(173).ToString());
				HTMLEntities.Add("&reg;", "®");
				HTMLEntities.Add("&macr;", "¯");
				HTMLEntities.Add("&deg;", "°");
				HTMLEntities.Add("&plusmn;", "±");
				HTMLEntities.Add("&sup2;", "²");
				HTMLEntities.Add("&sup3;", "³");
				HTMLEntities.Add("&acute;", "´");
				HTMLEntities.Add("&micro;", "µ");
				HTMLEntities.Add("&para;", "¶");
				HTMLEntities.Add("&middot;", "·");
				HTMLEntities.Add("&cedil;", "¸");
				HTMLEntities.Add("&sup1;", "¹");
				HTMLEntities.Add("&ordm;", "º");
				HTMLEntities.Add("&raquo;", " »");
				HTMLEntities.Add("&frac14;", "¼");
				HTMLEntities.Add("&frac12;", "½");
				HTMLEntities.Add("&frac34;", "¾");
				HTMLEntities.Add("&iquest;", "¿");
				HTMLEntities.Add("&Agrave;", "À");
				HTMLEntities.Add("&Aacute;", "Á");
				HTMLEntities.Add("&Acirc;", "Â");
				HTMLEntities.Add("&Atilde;", "Ã");
				HTMLEntities.Add("&Auml;", "Ä");
				HTMLEntities.Add("&Aring;", "Å");
				HTMLEntities.Add("&AElig;", "Æ");
				HTMLEntities.Add("&Ccedil;", "Ç");
				HTMLEntities.Add("&Egrave;", "È");
				HTMLEntities.Add("&Eacute;", "É");
				HTMLEntities.Add("&Ecirc;", "Ê");
				HTMLEntities.Add("&Euml;", "Ë");
				HTMLEntities.Add("&Igrave;", "Ì");
				HTMLEntities.Add("&Iacute;", "Í");
				HTMLEntities.Add("&Icirc;", "Î");
				HTMLEntities.Add("&Iuml;", "Ï");
				HTMLEntities.Add("&ETH;", "Ð");
				HTMLEntities.Add("&Ntilde;", "Ñ");
				HTMLEntities.Add("&Ograve;", "Ò");
				HTMLEntities.Add("&Oacute;", "Ó");
				HTMLEntities.Add("&Ocirc;", "Ô");
				HTMLEntities.Add("&Otilde;", "Õ");
				HTMLEntities.Add("&Ouml;", "Ö");
				HTMLEntities.Add("&times;", "×");
				HTMLEntities.Add("&Oslash;", "Ø");
				HTMLEntities.Add("&Ugrave;", "Ù");
				HTMLEntities.Add("&Uacute;", "Ú");
				HTMLEntities.Add("&Ucirc;", "Û");
				HTMLEntities.Add("&Uuml;", "Ü");
				HTMLEntities.Add("&Yacute;", "Ý");
				HTMLEntities.Add("&THORN;", "Þ");
				HTMLEntities.Add("&szlig;", "ß");
				HTMLEntities.Add("&agrave;", "à");
				HTMLEntities.Add("&aacute;", "á");
				HTMLEntities.Add("&acirc;", "â");
				HTMLEntities.Add("&atilde;", "ã");
				HTMLEntities.Add("&auml;", "ä");
				HTMLEntities.Add("&aring;", "å");
				HTMLEntities.Add("&aelig;", "æ");
				HTMLEntities.Add("&ccedil;", "ç");
				HTMLEntities.Add("&egrave;", "è");
				HTMLEntities.Add("&eacute;", "é");
				HTMLEntities.Add("&ecirc;", "ê");
				HTMLEntities.Add("&euml;", "ë");
				HTMLEntities.Add("&igrave;", "ì");
				HTMLEntities.Add("&iacute;", "í");
				HTMLEntities.Add("&icirc;", "î");
				HTMLEntities.Add("&iuml;", "ï");
				HTMLEntities.Add("&eth;", "ð");
				HTMLEntities.Add("&ntilde;", "ñ");
				HTMLEntities.Add("&ograve;", "ò");
				HTMLEntities.Add("&oacute;", "ó");
				HTMLEntities.Add("&ocirc;", "ô");
				HTMLEntities.Add("&otilde;", "õ");
				HTMLEntities.Add("&ouml;", "ö");
				HTMLEntities.Add("&divide;", "÷");
				HTMLEntities.Add("&oslash;", "ø");
				HTMLEntities.Add("&ugrave;", "ù");
				HTMLEntities.Add("&uacute;", "ú");
				HTMLEntities.Add("&ucirc;", "û");
				HTMLEntities.Add("&uuml;", "ü");
				HTMLEntities.Add("&yacute;", "ý");
				HTMLEntities.Add("&thorn;", "þ");
				HTMLEntities.Add("&yuml;", "ÿ");
				HTMLEntities.Add("&OElig;", "Œ");
				HTMLEntities.Add("&oelig;", "œ");
				HTMLEntities.Add("&Scaron;", "Š");
				HTMLEntities.Add("&scaron;", "š");
				HTMLEntities.Add("&Yuml;", "Ÿ");
				HTMLEntities.Add("&fnof;", "ƒ");
				HTMLEntities.Add("&circ;", "ˆ");
				HTMLEntities.Add("&tilde;", "˜");
				HTMLEntities.Add("&Alpha;", "Α");
				HTMLEntities.Add("&Beta;", "Β");
				HTMLEntities.Add("&Gamma;", "Γ");
				HTMLEntities.Add("&Delta;", "Δ");
				HTMLEntities.Add("&Epsilon;", "Ε");
				HTMLEntities.Add("&Zeta;", "Ζ");
				HTMLEntities.Add("&Eta;", "Η");
				HTMLEntities.Add("&Theta;", "Θ");
				HTMLEntities.Add("&Iota;", "Ι");
				HTMLEntities.Add("&Kappa;", "Κ");
				HTMLEntities.Add("&Lambda;", "Λ");
				HTMLEntities.Add("&Mu;", "Μ");
				HTMLEntities.Add("&Nu;", "Ν");
				HTMLEntities.Add("&Xi;", "Ξ");
				HTMLEntities.Add("&Omicron;", "Ο");
				HTMLEntities.Add("&Pi;", "Π");
				HTMLEntities.Add("&Rho;", "Ρ");
				HTMLEntities.Add("&Sigma;", "Σ");
				HTMLEntities.Add("&Tau;", "Τ");
				HTMLEntities.Add("&Upsilon;", "Υ");
				HTMLEntities.Add("&Phi;", "Φ");
				HTMLEntities.Add("&Chi;", "Χ");
				HTMLEntities.Add("&Psi;", "Ψ");
				HTMLEntities.Add("&Omega;", "Ω");
				HTMLEntities.Add("&alpha;", "α");
				HTMLEntities.Add("&beta;", "β");
				HTMLEntities.Add("&gamma;", "γ");
				HTMLEntities.Add("&delta;", "δ");
				HTMLEntities.Add("&epsilon;", "ε");
				HTMLEntities.Add("&zeta;", "ζ");
				HTMLEntities.Add("&eta;", "η");
				HTMLEntities.Add("&theta;", "θ");
				HTMLEntities.Add("&iota;", "ι");
				HTMLEntities.Add("&kappa;", "κ");
				HTMLEntities.Add("&lambda;", "λ");
				HTMLEntities.Add("&mu;", "μ");
				HTMLEntities.Add("&nu;", "ν");
				HTMLEntities.Add("&xi;", "ξ");
				HTMLEntities.Add("&omicron;", "ο");
				HTMLEntities.Add("&pi;", "π");
				HTMLEntities.Add("&rho;", "ρ");
				HTMLEntities.Add("&sigmaf;", "ς");
				HTMLEntities.Add("&sigma;", "σ");
				HTMLEntities.Add("&tau;", "τ");
				HTMLEntities.Add("&upsilon;", "υ");
				HTMLEntities.Add("&phi;", "φ");
				HTMLEntities.Add("&chi;", "χ");
				HTMLEntities.Add("&psi;", "ψ");
				HTMLEntities.Add("&omega;", "ω");
				HTMLEntities.Add("&thetasym;", "ϑ");
				HTMLEntities.Add("&upsih;", "ϒ");
				HTMLEntities.Add("&piv;", "ϖ");
				HTMLEntities.Add("&ensp", Convert.ToChar(8196).ToString());
				HTMLEntities.Add("&emsp", Convert.ToChar(8195).ToString());
				HTMLEntities.Add("&thinsp", Convert.ToChar(8201).ToString());
				HTMLEntities.Add("&zwnj", Convert.ToChar(8204).ToString());
				HTMLEntities.Add("&zwj", Convert.ToChar(8205).ToString());
				HTMLEntities.Add("&lrm", Convert.ToChar(8206).ToString());
				HTMLEntities.Add("&rlm", Convert.ToChar(8207).ToString());
				HTMLEntities.Add("&ndash;", "–");
				HTMLEntities.Add("&mdash;", "—");
				HTMLEntities.Add("&lsquo;", "‘");
				HTMLEntities.Add("&rsquo;", "’");
				HTMLEntities.Add("&sbquo;", "‚");
				HTMLEntities.Add("&ldquo;", "“");
				HTMLEntities.Add("&rdquo;", "”");
				HTMLEntities.Add("&bdquo;", "„");
				HTMLEntities.Add("&dagger;", "†");
				HTMLEntities.Add("&Dagger;", "‡");
				HTMLEntities.Add("&bull;", "•");
				HTMLEntities.Add("&hellip;", "…");
				HTMLEntities.Add("&permil;", "‰");
				HTMLEntities.Add("&prime;", "′");
				HTMLEntities.Add("&Prime;", "″");
				HTMLEntities.Add("&lsaquo;", "‹");
				HTMLEntities.Add("&rsaquo;", "›");
				HTMLEntities.Add("&oline;", "‾");
				HTMLEntities.Add("&frasl;", "⁄");
				HTMLEntities.Add("&euro;", "€");
				HTMLEntities.Add("&image;", "ℑ");
				HTMLEntities.Add("&weierp;", "℘");
				HTMLEntities.Add("&real;", "ℜ");
				HTMLEntities.Add("&trade;", "™");
				HTMLEntities.Add("&alefsym;", "ℵ");
				HTMLEntities.Add("&larr;", "←");
				HTMLEntities.Add("&uarr;", "↑");
				HTMLEntities.Add("&rarr;", "→");
				HTMLEntities.Add("&darr;", "↓");
				HTMLEntities.Add("&harr;", "↔");
				HTMLEntities.Add("&crarr;", "↵");
				HTMLEntities.Add("&lArr;", "⇐");
				HTMLEntities.Add("&uArr;", "⇑");
				HTMLEntities.Add("&rArr;", "⇒");
				HTMLEntities.Add("&dArr;", "⇓");
				HTMLEntities.Add("&hArr;", "⇔");
				HTMLEntities.Add("&forall;", "∀");
				HTMLEntities.Add("&part;", "∂");
				HTMLEntities.Add("&exist;", "∃");
				HTMLEntities.Add("&empty;", "∅");
				HTMLEntities.Add("&nabla;", "∇");
				HTMLEntities.Add("&isin;", "∈");
				HTMLEntities.Add("&notin;", "∉");
				HTMLEntities.Add("&ni;", "∋");
				HTMLEntities.Add("&prod;", "∏");
				HTMLEntities.Add("&sum;", "∑");
				HTMLEntities.Add("&minus;", "−");
				HTMLEntities.Add("&lowast;", "∗");
				HTMLEntities.Add("&radic;", "√");
				HTMLEntities.Add("&prop;", "∝");
				HTMLEntities.Add("&infin;", "∞");
				HTMLEntities.Add("&ang;", "∠");
				HTMLEntities.Add("&and;", "∧");
				HTMLEntities.Add("&or;", "∨");
				HTMLEntities.Add("&cap;", "∩");
				HTMLEntities.Add("&cup;", "∪");
				HTMLEntities.Add("&int;", "∫");
				HTMLEntities.Add("&there4;", "∴");
				HTMLEntities.Add("&sim;", "∼");
				HTMLEntities.Add("&cong;", "≅");
				HTMLEntities.Add("&asymp;", "≈");
				HTMLEntities.Add("&ne;", "≠");
				HTMLEntities.Add("&equiv;", "≡");
				HTMLEntities.Add("&le;", "≤");
				HTMLEntities.Add("&ge;", "≥");
				HTMLEntities.Add("&sub;", "⊂");
				HTMLEntities.Add("&sup;", "⊃");
				HTMLEntities.Add("&nsub;", "⊄");
				HTMLEntities.Add("&sube;", "⊆");
				HTMLEntities.Add("&supe;", "⊇");
				HTMLEntities.Add("&oplus;", "⊕");
				HTMLEntities.Add("&otimes;", "⊗");
				HTMLEntities.Add("&perp;", "⊥");
				HTMLEntities.Add("&sdot;", "⋅");
				HTMLEntities.Add("&lceil;", "⌈");
				HTMLEntities.Add("&rceil;", "⌉");
				HTMLEntities.Add("&lfloor;", "⌊");
				HTMLEntities.Add("&rfloor;", "⌋");
				HTMLEntities.Add("&lang;", "〈");
				HTMLEntities.Add("&rang;", "〉");
				HTMLEntities.Add("&loz;", "◊");
				HTMLEntities.Add("&spades;", "♠");
				HTMLEntities.Add("&clubs;", "♣");
				HTMLEntities.Add("&hearts;", "♥");
				HTMLEntities.Add("&diams;", "♦");
			}
		}
	}
}
