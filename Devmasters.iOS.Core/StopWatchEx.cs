﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Devmasters.Core
{

        public class StopWatchEx : Stopwatch
        {



            /// <summary>
            /// Gets the exact elapsed miliseconds.
            /// </summary>
            /// <value>The exact elapsed miliseconds.</value>
            public double ExactElapsedMiliseconds
            {

                get
                {

                    //long ticketsperMs = TimeSpan.TicksPerMillisecond ;

                    double d = (double)this.Elapsed.Ticks / (double)TimeSpan.TicksPerMillisecond;

                    return Math.Round(d, 5);

                }

            }

        }

}
