namespace Devmasters.Core
{
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Xml.Serialization;

    [Serializable]
    public class CryptedXMLSerializator : XMLSerializator
    {
        private static CryptoLib.KeySafe defaultCryptoKey = new CryptoLib.KeySafe("WOwpDG7ZKEKLfgy/eYLlDpKtSTObyU6d", "AJ2E5WoMJA8=", "TripleDES");

        public override string ToXml()
        {
            return this.ToXml(this.GetType());
        }

        protected override string ToXml(Type type)
        {
            return CryptedXMLSerializator.ToXml(this, type);
        }

        public new static object FromXml(string xml, Type type)
        {
            return FromXml(xml, type, defaultCryptoKey);
        }
        public static object FromXml(string xml, Type type, CryptoLib.KeySafe key)
        {
            if (xml == null)
                return null;
            CryptoLib.TripleDES des3 = new Devmasters.Core.CryptoLib.TripleDES(key);            
            string decryptXML = des3.Decrypt(xml);
            return XMLSerializator.FromXml(decryptXML, type);
        }

        public new static string ToXml(object Obj, Type type)
        {
            return ToXml(Obj, type, defaultCryptoKey);
        }

        public static string ToXml(object Obj, Type type, CryptoLib.KeySafe key)
        {
            CryptoLib.TripleDES des3 = new Devmasters.Core.CryptoLib.TripleDES(key);

            return des3.EncryptString(XMLSerializator.ToXml(Obj, type));

        }

    }
}

