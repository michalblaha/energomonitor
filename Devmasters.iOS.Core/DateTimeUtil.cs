﻿using System;
using System.Globalization;

namespace Devmasters.Core
{
	public enum DateTimeDiff
	{
		Day,
		Hour
	}

	public static class DateTimeUtil
	{

		public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
		{
			int diff = dt.DayOfWeek - startOfWeek;
			if (diff < 0)
			{
				diff += 7;
			}

			return dt.AddDays(-1 * diff).Date;
		}
		public static DateTime FromJavascriptEpochTime(string msSinceEpoch)
		{
			return FromJavascriptEpochTime(Convert.ToDouble(msSinceEpoch));
		}

		public static DateTime FromJavascriptEpochTime(long msSinceEpoch)
		{
			return FromJavascriptEpochTime(Convert.ToDouble(msSinceEpoch));
		}

		public static DateTime FromJavascriptEpochTime(double msSinceEpoch)
		{
			return DateTime.MinValue.AddYears (1969).AddMilliseconds (msSinceEpoch * 1000d);
		}
        public static int ToUnixTimeInSec(this DateTime dateInUTC)
        {
            return
                System.Convert.ToInt32(
                (dateInUTC - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds)
                ;
        }

		public static int Diff(this DateTime? instance, DateTime date, DateTimeDiff compare)
		{
			if (instance == null)
				return 0;

			switch (compare)
			{
				case DateTimeDiff.Day:
					{
						TimeSpan span = date.Subtract(instance.Value);
						return span.Days;
					}

				case DateTimeDiff.Hour:
					{
						TimeSpan span = date.Subtract(instance.Value);

						int hours = span.Hours;
						hours += span.Days * 24;

						return hours;
					}
			}

			return 0;
		}

		public static DateTime? ParseDateTime(string date, DateTime? valueIfNull)
		{
			DateTime parsed;

			return DateTime.TryParse(date, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out parsed) ? parsed : valueIfNull;
		}


		public static DateTime SqlMinDate(this DateTime value)
		{
			return new DateTime(1753, 1, 1); // 01/01/1753 
		}

		public static bool IsSqlMinDate(this DateTime value)
		{
			return (value.CompareTo(DateTime.UtcNow.SqlMinDate()) == 0); // 01/01/1753 
		}

		public static DateTime SqlMaxDate(this DateTime value)
		{
			return new DateTime(9999, 12, 31, 23, 59, 59, 997); //9999-12-31 23:59:59.997 
		}

		public static bool IsSqlMaxDate(this DateTime value)
		{
			return (value.CompareTo(DateTime.UtcNow.SqlMaxDate()) == 0); // 01/01/1753 
		}

		public static string Ago(this DateTime date)
		{
			return date.Ago(System.Threading.Thread.CurrentThread.CurrentUICulture);
		}
		public static string Ago(this DateTime date, CultureInfo culture)
		{
            Devmasters.Core.Manager rm = new Devmasters.Core.Manager();
			string timeago = string.Empty;

			TimeSpan ts = DateTime.UtcNow - date.ToUniversalTime();
            int absDays = Math.Abs(ts.Days);
            int absHours = Math.Abs(ts.Hours);
			if (absDays < 1 && absHours < 6) //near
			{
                if (ts.TotalHours<0)
                    timeago = rm.TextReader.GetString("InHours" + (-ts.Hours).ToString(), culture);
                else
    				timeago = rm.TextReader.GetString("HoursAgo" + ts.Hours.ToString(), culture);
			}
			else if (absDays < 8)
			{
                if (ts.TotalHours < 0)
                    timeago = rm.TextReader.GetString("InDays" + (-ts.Days).ToString(), culture);
                else
                    timeago = rm.TextReader.GetString("DaysAgo" + ts.Days.ToString(), culture);
            }
			else
                if (ts.TotalHours < 0)
                    timeago = string.Format(rm.TextReader.GetString("InDaysMore", culture), -ts.Days);
                else
                    timeago = string.Format(rm.TextReader.GetString("DaysAgoMore", culture), ts.Days);


			return timeago;
		}

		public static string GetFormatedDateForTrip(DateTime? departureDate, DateTime? returnDate, DateTime? firstArticleDate)
		{
			return GetFormatedDateForTrip(departureDate, returnDate, firstArticleDate, System.Globalization.CultureInfo.CurrentUICulture);
		}

		public static string GetFormatedDateForTrip(DateTime? departureDate, DateTime? returnDate, DateTime? firstArticleDate, System.Globalization.CultureInfo culture)
		{
            Devmasters.Core.Manager rm = new Devmasters.Core.Manager();
			if (culture.IsNeutralCulture)
				culture = Devmasters.Core.TextUtil.GetSpecificCulture(culture);
			string ret = string.Empty;
			if (departureDate != null || returnDate != null)
			{
				if (departureDate != null)
					if (returnDate == null)
						ret = string.Format(rm.TextReader.GetString("SinceDate",culture), departureDate.Value.ToShortDateString());
					else
						ret += departureDate.Value.ToShortDateString() + " ~ " + returnDate.Value.ToShortDateString();
				else if (returnDate != null)
					ret = returnDate.Value.ToShortDateString();
			}
			else
			{
				if (firstArticleDate != null)
					ret = string.Format(rm.TextReader.GetString("SinceDate", culture), firstArticleDate.Value.ToShortDateString());
				else
					ret = rm.TextReader.GetString("NotStartedYet", culture);
			}
			return ret;

		}


		static NumberFormatInfo nfi = new NumberFormatInfo() { NumberDecimalSeparator = ":" };
		public static string ToGMTString(this TimeZoneInfo timezone)
		{
            
			string tzn = "GMT";
            if (timezone.BaseUtcOffset.Hours > 0)
				tzn += "+" + string.Format("{0:00}:{1:00}", timezone.BaseUtcOffset.Hours, timezone.BaseUtcOffset.Minutes);
            else if (timezone.BaseUtcOffset.Hours < 0)
                tzn += string.Format("{0:00}:{1:00}", timezone.BaseUtcOffset.Hours, Math.Abs(timezone.BaseUtcOffset.Minutes));
            return tzn;		
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="date"></param>
		/// <param name="template">Have to contains {0}{1}{2}</param>
		/// <param name="timezone"></param>
		/// <returns></returns>
		public static string ToFormatedStringWithTimezone(DateTime? date, string template, TimeZoneInfo timezone, bool showTimezone, CultureInfo culture) 
		{
			if (!date.HasValue)
				return string.Empty;

			if (culture.IsNeutralCulture)
				culture = Devmasters.Core.TextUtil.GetSpecificCulture(culture);

			string publishDate = date.Value.ToString("d",culture);
			string publishTime = date.Value.ToString("t", culture);

			if (showTimezone)
			{
				
				return string.Format(template,
						publishDate,
						publishTime,
						timezone.ToGMTString()
					);
			}
			else
				return string.Format(template,
						publishDate,
						publishTime,
						string.Empty
					);

		}

		public static string ToFormatedStringWithTimezone(DateTime? date, string template, TimeZoneInfo timezone, bool showTimezone)
		{
			return ToFormatedStringWithTimezone(date, template, timezone, showTimezone, System.Threading.Thread.CurrentThread.CurrentUICulture);
		}

		public static string ToFormatedStringWithTimezone(DateTime? date, TimeZoneInfo timezone, bool showTimezone)
		{
			return ToFormatedStringWithTimezone(date, "{0} {1} {2}", timezone, showTimezone, System.Threading.Thread.CurrentThread.CurrentUICulture);
		}

		public static TimeZoneInfo GMT_TimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

	}
}