﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Devmasters.Core
{
    public static class CultureInfoFix
    {
        public static CultureInfo FromString(string langCode)
        {
			if (String.IsNullOrEmpty(langCode))
				return null;

            // iPhone Fix
            if (langCode.IndexOf('_') != -1)
                langCode = langCode.Replace('_', '-');

            Regex longVariant = new Regex(@"^[a-z]+-[A-Z]+$");
            Regex shortVariant = new Regex(@"^[a-z]+$");

            try
            {
                if (longVariant.IsMatch(langCode.Trim()))
                    return new CultureInfo(langCode);

                if (shortVariant.IsMatch(langCode.Trim()))
                    return new CultureInfo(langCode);
            }
            catch (ArgumentException)
            {
                Devmasters.Core.Logger.Warn(String.Format("GetSearchCategories: Invalid culture code {0}.", langCode));
                return null;
            }

            return null;
        }
    }
}
