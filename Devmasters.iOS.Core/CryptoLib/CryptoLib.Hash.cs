using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Devmasters.Core.CryptoLib
{
	public class Hash
	{

		public static string ComputeHash(string s)
		{

			// P�ev�st vstupn� �et�zec na pole bajt�
			byte[] data = System.Text.Encoding.UTF8.GetBytes(s);

			// Spo��tat MD5 hash
			string r;
			using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
			{
				byte[] hash = md5.ComputeHash(data);

				// P�ev�st na Base64
				r = Convert.ToBase64String(hash);
			}

			return r;
		}


		public static string ComputeHashToHex(string s)
		{

			// P�ev�st vstupn� �et�zec na pole bajt�
			byte[] data = System.Text.Encoding.UTF8.GetBytes(s);

			// Spo��tat MD5 hash
			using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
			{
				byte[] hash = md5.ComputeHash(data);
				// Build the final string by converting each byte
				// into hex and appending it to a StringBuilder
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hash.Length; i++)
				{
					sb.Append(hash[i].ToString("X2"));
				}

				// And return it
				return sb.ToString();
			}

		}


	}
}
