using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using System.Text.RegularExpressions;
using Devmasters.Core;

namespace Devmasters.Core.Web
{

    /// <summary>
    /// Allowed types of HTTP Request
    /// </summary>
    public enum MethodEnum
    {
        GET,
        POST
    }

    /// <summary>
    /// Downloads content of URL page (usefull only for text content, not for binary content)
    /// </summary>
    public class URLContent : IDisposable
    {
        private string _userAgent = GetUserAgent();
        private string _charset;
        private string _url;
        private TimeSpan _time = new TimeSpan(0);
        private string _contentType;
        private int _timeout = 60000;
        private NetworkCredential _credentials = null;
        private IWebProxy _proxy = WebRequest.DefaultWebProxy;
        HttpWebResponse _result = null;
        HttpWebRequest _request = null;
        MethodEnum _method = MethodEnum.GET;
        ParametersContainer _requestParams = new ParametersContainer();
        ParametersContainer _responseParams = null;

        bool _orig_Expect100Continue = System.Net.ServicePointManager.Expect100Continue;
        bool _orig_UseNagleAlgorithm = System.Net.ServicePointManager.UseNagleAlgorithm;


        /// <summary>
        /// Initializes a new instance of the class, for URL with parameters in URL defined in queryString value
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="queryString"></param>
        public URLContent(string URL, System.Collections.Specialized.NameValueCollection queryString)
        {
            System.Text.StringBuilder tmpURL = new StringBuilder(URL);
            if (!URL.Contains("?"))
            {
                tmpURL.Append("?");
            }
            foreach (string key in queryString)
            {
                tmpURL.Append(key);
                tmpURL.Append("=");
                tmpURL.Append(System.Web.HttpUtility.UrlEncode(queryString[key]));
                tmpURL.Append("&");
            }
            _url = tmpURL.ToString();
            _request = (HttpWebRequest)HttpWebRequest.Create(_url);
        }

        public string Url
        {
            get
            {
                return _url;
            }
        }

        /// <summary>
        /// Initializes a new instance of the class, for URL
        /// </summary>
        /// <param name="URL"></param>
        public URLContent(string URL)
        {
            _url = URL;
            _request = (HttpWebRequest)HttpWebRequest.Create(_url);
        }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        /// <summary>
        /// Get or set UserAgent
        /// </summary>
        public string UserAgent
        {
            get { return _userAgent; }
            set { _userAgent = value; }
        }

        /// <summary>
        /// Get or set ParametersContainer for Request
        /// </summary>
        public ParametersContainer RequestParams
        {
            get { return _requestParams; }
            set { _requestParams = value; }
        }

        /// <summary>
        /// Get ParametersContainer for HTTP Response.
        /// Its always null before you call GetContent method
        /// </summary>
        public ParametersContainer ResponseParams
        {
            get { return _responseParams; }
        }

        /// <summary>
        /// Get or set ParametersContainer for Request
        /// </summary>
        public IWebProxy Proxy
        {
            get { return _proxy; }
            set { _proxy = value; }
        }

        /// <summary>
        /// Accessor for HTTPWebResponse object
        /// Its always null before you call GetContent method
        /// </summary>
        public HttpWebResponse Response
        {
            get { return _result; }
        }

        /// <summary>
        /// Accessor for HTTPWebRequest object. Don't use after you call GetContent method
        /// </summary>
        public HttpWebRequest Request
        {
            get { return _request; }
        }

        /// <summary>
        /// Get ContentType of page
        /// </summary>
        public string ContentType
        {
            get
            {
                return _contentType;
            }
        }

        /// <summary>
        /// How long was content downloaded and processed. Before calling GetContent method its always 0 ticks.
        /// </summary>
        public TimeSpan ProcessTime
        {
            get
            {
                return _time;
            }
        }

        /// <summary>
        /// Get or set GET or POST method for request
        /// </summary>
        public MethodEnum Method
        {
            get { return _method; }
            set
            {
                _method = value;
                if (_method == MethodEnum.POST)
                    EnablePostConfig();
                else
                    DisablePostConfig();
            }
        }

        /// <summary>
        /// Set all parameters for POST request
        /// </summary>
        private void EnablePostConfig()
        {
            System.Net.ServicePointManager.Expect100Continue = false;
            System.Net.ServicePointManager.UseNagleAlgorithm = false;
            _contentType = "application/x-www-form-urlencoded";

        }

        /// <summary>
        /// Set all parameters for GET method (to values in time of initialization of class or before POST request)
        /// </summary>
        private void DisablePostConfig()
        {
            System.Net.ServicePointManager.Expect100Continue = _orig_Expect100Continue;
            System.Net.ServicePointManager.UseNagleAlgorithm = _orig_UseNagleAlgorithm;
            _contentType = "";

        }

        /// <summary>
        /// Get default UserAgent for request
        /// </summary>
        /// <returns></returns>
        public static string GetUserAgent()
        {
            return GetUserAgent("");
        }

        /// <summary>
        /// Get some of prepared UserAgents
        /// </summary>
        /// <param name="browser"></param>
        /// <returns></returns>
        public static string GetUserAgent(string browser)
        {
            browser = browser.ToLower();
            string Mozilla = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;)";
            string FF2 = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.8.1b2) Gecko/20060821 Firefox/2.0";
            string IE6 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            string IE7 = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";

            switch (browser)
            {
                case "ff2":
                    return FF2;
                case "ie6":
                    return IE6;
                case "ie7":
                    return IE7;
                case "mozilla":
                    return Mozilla;
                default:
                    return Mozilla;
            }

        }

        /// <summary>
        /// Set basic credentials for request
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void SetCredentials(string username, string password)
        {
            _credentials = new NetworkCredential(username, password);

        }

        /// <summary>
        /// Set basic credentials for request, included domain
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="domain"></param>
        public void SetCredentials(string username, string password, string domain)
        {
            _credentials = new NetworkCredential(username, password, domain);
        }


        /// <summary>
        /// Get content of page in defined charset 
        /// </summary>
        /// <param name="charset"></param>
        /// <returns></returns>
        public virtual string GetContent(string charset)
        {
            string content;
            StopWatchEx stopwatch = new StopWatchEx();
            stopwatch.Start();
            _charset = charset;
            content = FillContent(_url, charset);
            stopwatch.Stop();
            _time = stopwatch.Elapsed;
            return content;
        }

        public virtual byte[] GetBinary()
        {
            StopWatchEx stopwatch = new StopWatchEx();
            stopwatch.Start();
            byte[] data = null;
            data = GetBinaryContent(_url);
            stopwatch.Stop();
            _time = stopwatch.Elapsed;
            return data;
        }

        /// <summary>
        /// Get content of page, try to find out charset automatically
        /// </summary>
        /// <returns></returns>
        public virtual string GetContent()
        {
            return GetContent("");
        }

        #region "private"

        /// <summary>
        /// Find ouf charset of page from HTTP Headers or first 4 kB of request
        /// </summary>
        /// <param name="result"></param>
        /// <param name="ReceiveStream"></param>
        /// <returns></returns>
        private string GetCharset(HttpWebResponse result, MemoryStream ReceiveStream)
        {
            int size = 4 * 1024;
            HttpWebResponse _result = result;
            StringBuilder ContentSB = new StringBuilder(size);
            string Content;
            string sCharsetRegex = "(?ixm-s)(charset=(?<charset>[A-Za-z\\-0-9_]*)|encoding=\\W*(?<charset>[A-Za-z\\-0-9_]*))";
            Regex FoundRegex;
            MatchCollection found;
            ArrayList _tmpContent = new ArrayList(200);
            if (_result == null)
                return "ASCII";
            try
            {
                ReceiveStream.Seek(0, SeekOrigin.Begin);

                string sHeaderValue;
                string sCharset;
                _contentType = _result.ContentType;
                foreach (string sHeaderKey in _result.Headers)
                {
                    if (sHeaderKey.ToLower().Contains("content-type"))
                    {
                        sHeaderValue = _result.Headers[sHeaderKey];
                        FoundRegex = new Regex(sCharsetRegex, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace);
                        found = FoundRegex.Matches(sHeaderValue);
                        foreach (Match aMatch in found)
                        {

                            sCharset = aMatch.Groups["charset"].Value.Trim();
                            if (!string.IsNullOrEmpty(sCharset))
                            {
                                try
                                {
                                    Encoding _enc = System.Text.Encoding.GetEncoding(sCharset);
                                    return sCharset;
                                }
                                catch
                                {
									//Logger.Debug("No valid charset identified in headers");
                                }
                            }
                        }
                    }
                }

                byte[] read = new byte[size];
                Content = "";
                Encoding encode = System.Text.Encoding.Default;
                System.IO.BinaryReader readStream = new System.IO.BinaryReader(ReceiveStream);
                int count;
                count = readStream.Read(read, 0, size);
                for (int i = 0; i <= count - 1; i++)
                {
                    ContentSB.Append(Util.Chr(read[i]));
                }
                Content = ContentSB.ToString();
                if (_charset == "")
                {
                    Content = Util.Left(Content, size);
                    FoundRegex = new Regex(sCharsetRegex, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace);
                    found = FoundRegex.Matches(Content);
                    foreach (Match aMatch in found)
                    {
                        sCharset = aMatch.Groups["charset"].Value.Trim();
                        if (sCharset != "")
                        {
                            try
                            {
                                Encoding _enc = System.Text.Encoding.GetEncoding(sCharset);
                                return sCharset;
                            }
                            catch
                            {
								//Logger.Debug("No valid charset identified in HTML");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
				//Logger.Debug("Some exception occured in time of searching charset", ex);
                _charset = "ASCII";
            }
            finally
            { }

            if (_charset == "")
            {
                _charset = "ASCII";
            }
            return _charset;
        }

        private string FillContent(string URL, string charset)
        {
            string downloadedcontent = string.Empty;
            Stream ReceiveStream = null;
            BinaryReader readStream = null;
            StreamReader sr = null;
            try
            {

                //Logger.Debug("Request stream parameters setting");
                //set request parameters before calling HTTP request
                if (_requestParams != null)
                {
					CookieContainer cc = new CookieContainer();
					cc.Add(_requestParams.Cookies);
					_request.CookieContainer = cc;
                    foreach (string key in _requestParams.Headers.AllKeys)
                    {
                        _request.Headers.Add(key, _requestParams.Headers[key]);
                    }
                    //_request.Headers = _requestParams.Headers;
                }
                _request.UserAgent = _userAgent;
                _request.Timeout = _timeout;
                //_request.Proxy = _proxy;
                _request.Method = _method.ToString();
                // _request.Credentials = _credentials;
                if (_credentials != null && !string.IsNullOrEmpty(_credentials.UserName) && !string.IsNullOrEmpty(_credentials.Password))
                {
                    string ticket = string.Format("{0}:{1}", _credentials.UserName, _credentials.Password);
                    _request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(ticket)));
                }

                _request.ContentType = _contentType;

                //if POST, add post data into request stream
                if (_method == MethodEnum.POST)
                {
                    Stream streamReq = _request.GetRequestStream();
                    string postData = RequestParams.FormInPostDataFormat();
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    streamReq.Write(byteArray, 0, postData.Length);
                    streamReq.Close();
                }


                //Logger.Debug("Request started, loading data");

                //Open request, load data
                _result = (HttpWebResponse)_request.GetResponse();
                ReceiveStream = _result.GetResponseStream();

                MemoryStream memStream = new MemoryStream();
                readStream = new BinaryReader(ReceiveStream);


                byte[] read = new byte[2048];
                int count = readStream.Read(read, 0, read.Length);
                while (count > 0)
                {
                    memStream.Write(read, 0, count);
                    count = readStream.Read(read, 0, read.Length);
                }
				_responseParams = new ParametersContainer(_result.Headers, _result.Cookies);

                if (charset == "")
                    _charset = GetCharset(_result, memStream);

                //back to begin of stream
                memStream.Seek(0, SeekOrigin.Begin);
                sr = new StreamReader(memStream, System.Text.Encoding.GetEncoding(_charset));
                downloadedcontent = sr.ReadToEnd();

            }
            catch (WebException ex)
            {
                string exdownloadedcontent = string.Empty;
                if (ex.Response != null)
                {
                    MemoryStream exmemStream = new MemoryStream();
                    if (ex.Response.GetResponseStream() != null && ex.Response.GetResponseStream().CanRead)
                    {
                        BinaryReader exreadStream = new BinaryReader(ex.Response.GetResponseStream());
                        byte[] exread = new byte[2048];
                        int count = exreadStream.Read(exread, 0, exread.Length);
                        while (count > 0)
                        {
                            exmemStream.Write(exread, 0, count);
                            count = exreadStream.Read(exread, 0, exread.Length);
                        }
                        string excharset = GetCharset(_result, exmemStream);
                        //back to begin of stream
                        exmemStream.Seek(0, SeekOrigin.Begin);
                        StreamReader exsr = new StreamReader(exmemStream, System.Text.Encoding.GetEncoding(excharset));
                        exdownloadedcontent = exsr.ReadToEnd();
                        if (exsr != null)
                            exsr.Close();
                        //close memory stream
                        if (exreadStream != null)
                            exreadStream.Close();
                    }
                }
                Logger.Error(this.ToString(), ex);
                //throw new WebException(exdownloadedcontent, ex);
            }
            catch (Exception exc)
            {
                Logger.Error(this.ToString(), exc);
                throw exc;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                //close memory stream
                if (readStream != null)
                    readStream.Close();

                if (!(_result == null))
                {
                    ReceiveStream.Close();
                }
                //Logger.Debug("Request stream closed");
            }

            return downloadedcontent;
        }

        private byte[] GetBinaryContent(string URL)
        {
            Stream ReceiveStream = null;
            BinaryReader readStream = null;
            try
            {

                //Logger.Debug("Request stream parameters setting");
                //set request parameters before calling HTTP request
                if (_requestParams != null)
                {
					CookieContainer cc = new CookieContainer();
					cc.Add(_requestParams.Cookies);
					_request.CookieContainer = cc;
                    foreach (string key in _requestParams.Headers.AllKeys)
                    {
                        _request.Headers.Add(key, _requestParams.Headers[key]);
                    }
                    //_request.Headers = _requestParams.Headers;
                }
                _request.UserAgent = _userAgent;
                _request.Timeout = _timeout;
                _request.Proxy = _proxy;
                _request.Method = _method.ToString();
                // _request.Credentials = _credentials;
                if (_credentials != null && !string.IsNullOrEmpty(_credentials.UserName) && !string.IsNullOrEmpty(_credentials.Password))
                {
                    string ticket = string.Format("{0}:{1}", _credentials.UserName, _credentials.Password);
                    _request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(ticket)));
                }

                _request.ContentType = _contentType;

                //if POST, add post data into request stream
                if (_method == MethodEnum.POST)
                {
                    Stream streamReq = _request.GetRequestStream();
                    string postData = RequestParams.FormInPostDataFormat();
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    streamReq.Write(byteArray, 0, postData.Length);
                    streamReq.Close();
                }


                //Logger.Debug("Request started, loading data");

                //Open request, load data
                _result = (HttpWebResponse)_request.GetResponse();
                ReceiveStream = _result.GetResponseStream();

                MemoryStream memStream = new MemoryStream();
                readStream = new BinaryReader(ReceiveStream);


                byte[] read = new byte[2048];
                int count = readStream.Read(read, 0, read.Length);
                while (count > 0)
                {
                    memStream.Write(read, 0, count);
                    count = readStream.Read(read, 0, read.Length);
                }
				_responseParams = new ParametersContainer(_result.Headers, _result.Cookies);


                //back to begin of stream
                memStream.Seek(0, SeekOrigin.Begin);
                return memStream.ToArray();

            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    MemoryStream exmemStream = new MemoryStream();
                    if (ex.Response.GetResponseStream() != null && ex.Response.GetResponseStream().CanRead)
                    {
                        BinaryReader exreadStream = new BinaryReader(ex.Response.GetResponseStream());
                        byte[] exread = new byte[2048];
                        int count = exreadStream.Read(exread, 0, exread.Length);
                        while (count > 0)
                        {
                            exmemStream.Write(exread, 0, count);
                            count = exreadStream.Read(exread, 0, exread.Length);
                        }

                        //back to begin of stream
                        exmemStream.Seek(0, SeekOrigin.Begin);
                        byte[] data = exmemStream.ToArray();
                        //close memory stream
                        if (exreadStream != null)
                            exreadStream.Close();
                    }
                }
                throw new WebException(this.ToString(), ex);
            }
            catch (Exception exc)
            {
                Logger.Error(this.ToString(), exc);
                throw exc;
            }
            finally
            {
                //close memory stream
                if (readStream != null)
                    readStream.Close();

                if (!(_result == null))
                {
                    ReceiveStream.Close();
                }
                //Logger.Debug("Request stream closed");
            }

        }
        public override string ToString()
        {
            string template = "URL: {0}\nPost:{1}";
            return string.Format(template, this._url, CollectionToString(this.RequestParams.Form));

            //return base.ToString();
        }

        private string CollectionToString(System.Collections.Specialized.NameValueCollection coll)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in coll)
            {
                sb.AppendLine(key + " - " + coll[key]);
            }
            return sb.ToString();
        }
        #endregion


        #region IDisposable Members

        public void Dispose()
        {
            if (_method == MethodEnum.POST)
            {
                //clear special config for POSTing data
                DisablePostConfig();
            }

            if (_result != null)
                _result.Close();

            if (_request != null)
            {
                _request = null;
            }
        }

        #endregion
    }
}

