using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Web;
using System.Net;
using System.Net.Mail;


namespace Devmasters.Core
{

	/// <summary>
	/// Log events of application with Log4net library
	/// </summary>
	public static class Logger2
	{
		private const string CACHE_AllAssemblyVersions = "AllAssemblyVersions";
		private const string CONFIG_FILENAME = "Logger.log4net";
        public static log4net.ILog Root = log4net.LogManager.GetLogger("Devmasters.Core");

        public static log4net.ILog PageTimes = log4net.LogManager.GetLogger("PageTimes");


		static Logger2()
		{
			//string rootDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location ) + @"\";
			string rootDir = AppDomain.CurrentDomain.BaseDirectory;
			if (System.IO.File.Exists(rootDir + CONFIG_FILENAME))
			{
				log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(rootDir + CONFIG_FILENAME));
			}
			else
			{
				throw new ApplicationException(rootDir + CONFIG_FILENAME + " was not found!");
			}
		}

		/// <summary>
		/// Returns name of method, from which is this method called.
		/// </summary>
		/// <returns></returns>
		public static string GetCallingMethod(bool showFullStack)
		{
			int startFrame = 1;
			StackFrame stackframe;
			StringBuilder sb = new StringBuilder(1024);
			do
			{
				stackframe = new StackFrame(startFrame, true);
				if (showFullStack)
                    sb.Append(FormatStackFrame(stackframe));
				startFrame++;
			} while (stackframe.GetMethod() != null); //&& stackframe.GetMethod().ReflectedType.FullName == "Devmasters.Core.Logger");
			if (stackframe != null)
			{
				sb.Append(FormatStackFrame(stackframe));
				if (showFullStack)
					do
					{
						stackframe = new StackFrame(startFrame, true);
						sb.Append(FormatStackFrame(stackframe));
						startFrame++;
					} while (stackframe.GetMethod() != null);
				return sb.ToString();
			}
			else
				return "Unknown method";
		}

		private static string FormatStackFrame(StackFrame stackframe)
		{
			if (stackframe == null || stackframe.GetMethod() == null)
				return string.Empty;
			else
				return (
					string.Format("{0}.{1} (line {2}, col {3} in {4})\n",
						stackframe.GetMethod().ReflectedType.FullName,
						stackframe.GetMethod().Name ,
						stackframe.GetFileLineNumber().ToString(),
						stackframe.GetFileColumnNumber(),
						stackframe.GetFileName()
						)
					);
			//return stackframe.ToString();

		}
		/// <summary>
		/// Logs error in application
		/// </summary>
		/// <param name="mess"></param>
		/// <param name="ex"></param>
		public static void Error(this log4net.ILog Log, string mess, Exception ex)
		{
			Log.Error(string.Format("{0} {1}", GetCallingMethod(true), mess), ex);
		}
        public static void Error(this log4net.ILog Log, string mess)
		{
			Log.Error(string.Format("{0} {1}", GetCallingMethod(true), mess));
		}
        public static void Error(this log4net.ILog Log, Exception ex)
		{
			Error(Log,"", ex);
		}

		/// <summary>
		/// Logs warning in application
		/// </summary>
		/// <param name="mess"></param>
		/// <param name="ex"></param>
        public static void Warn(this log4net.ILog Log, string mess, Exception ex)
		{
			Log.Warn(string.Format("{0} {1}", GetCallingMethod(true), mess), ex);
		}
        public static void Warn(this log4net.ILog Log, string mess)
		{
			Log.Warn(string.Format("{0} {1}", GetCallingMethod(true), mess));
		}
        public static void Warn(this log4net.ILog Log, Exception ex)
		{
			Warn(Log,"", ex);
		}


		/// <summary>
		/// Logs info in application
		/// </summary>
		/// <param name="mess"></param>
		/// <param name="ex"></param>
        public static void Info(this log4net.ILog Log, string mess, Exception ex)
		{
			Log.Info(string.Format("{0} {1}", GetCallingMethod(false), mess), ex);
		}
        public static void Info(this log4net.ILog Log, string mess)
		{
			Log.Info(string.Format("{0} {1}", GetCallingMethod(false), mess));
		}
        public static void Info(this log4net.ILog Log, Exception ex)
		{
			Info(Log,"", ex);
		}

		/// <summary>
		/// Logs Fatal errors in application
		/// </summary>
		/// <param name="mess"></param>
		/// <param name="ex"></param>
        public static void Fatal(this log4net.ILog Log, string mess, Exception ex)
		{
			Log.Fatal(string.Format("{0} {1}", GetCallingMethod(true), mess), ex);
		}
        public static void Fatal(this log4net.ILog Log, string mess)
		{
			Log.Fatal(string.Format("{0} {1}", GetCallingMethod(true), mess));
		}
        public static void Fatal(this log4net.ILog Log, Exception ex)
		{
			Fatal(Log,"", ex);
		}

		/// <summary>
		/// Logs debug messages in application
		/// </summary>
		/// <param name="mess"></param>
		/// <param name="ex"></param>
        public static void Debug(this log4net.ILog Log, string mess, Exception ex)
		{
			Log.Debug(string.Format("{0} {1}", GetCallingMethod(false), mess), ex);
		}
        public static void Debug(this log4net.ILog Log, string mess)
		{
			Log.Debug(string.Format("{0} {1}", GetCallingMethod(false), mess));
		}
        public static void Debug(this log4net.ILog Log, Exception ex)
		{
			Debug(Log, "", ex);
		}

		static string versions = null;
		static DateTime versionsLastUpdate = DateTime.MinValue;
		public static string Versions()
		{

			if (versionsLastUpdate > DateTime.MinValue) {
				if ((DateTime.Now - versionsLastUpdate).TotalHours > 24)
					versions = null;

			}
			if (versions == null)
			{
				string ver = "";
				System.Reflection.Assembly[] ass = AppDomain.CurrentDomain.GetAssemblies();
				foreach (System.Reflection.Assembly a in ass)
				{
					if (a.ManifestModule.Name.ToLower().StartsWith("devmasters."))
					{
						ver += string.Format("{0}:{1}; ", a.ManifestModule.Name, a.GetName().Version.ToString());
					}

				}
				versions = ver;	
				versionsLastUpdate = DateTime.Now;
			}
			return versions;
		}




	}
}
