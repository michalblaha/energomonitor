﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Devmasters.Core
{
    public class SafeTextNumeralBase : Numeral
    {
        public SafeTextNumeralBase()
            : base(new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' })
        { }
    }
}
