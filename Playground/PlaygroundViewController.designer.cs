// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace Playground
{
	[Register ("PlaygroundViewController")]
	partial class PlaygroundViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITextView txtLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtLabel != null) {
				txtLabel.Dispose ();
				txtLabel = null;
			}
		}
	}
}
