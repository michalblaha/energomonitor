﻿using System;
using System.Collections.Generic;

namespace Energomonitor.Lib.iOS.Api.JSON
{
	public class Config
	{

		public class Urls
		{
			public string version { get; set; }
			public string data { get; set; }
			public string img { get; set; }
		}

		public class Feed
		{
			public List<string> panels { get; set; }
			public object end { get; set; }
			public string label { get; set; }
			public object start { get; set; }
			public string timezone { get; set; }
			public string id { get; set; }
		}

		public class Params
		{
			public string feed_id { get; set; }
			public object stream { get; set; }
			public string type { get; set; }
			public string primary { get; set; }
		}

		public class Widget
		{
			public string widget { get; set; }
			public string type { get; set; }
			public string primary {get;set;}
			public Params @params { get; set; }
		}

		public class Panel
		{
			public string url { get; set; }
			public List<Widget> widgets { get; set; }
			public string controller { get; set; }
			public string id { get; set; }
			public string title { get; set; }
		}

		public string lang { get; set; }
		public string user { get; set; }
		public Urls urls { get; set; }
		public int first_wday { get; set; }
		public string timezone { get; set; }
		public List<Feed> feeds { get; set; }
		public List<Panel> panels { get; set; }

		public Config ()
		{
		}
	}
}

