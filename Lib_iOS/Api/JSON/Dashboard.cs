﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Energomonitor.Lib.iOS.Api.JSON
{
	public class Dashboard
	{
		Config config = null;
		public Dashboard (Config config)
		{
			this.config = config;
		}

		public List<Config.Widget> Widgets()
		{
			var w = config.panels
				.Where (m => m.url == "dashboard")
				.FirstOrDefault();
			if (w != null)
				return w.widgets;
			else
				return null;

		}
	}
}

