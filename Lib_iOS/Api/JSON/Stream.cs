﻿using System;
using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Energomonitor.Lib.iOS.Api.JSON
{
	public class Stream
	{
		public class Data
		{
			public System.DateTime date { get; set;}
			public double value { get; set; }
		}

			public string style { get; set; }
			public string medium { get; set; }
			public object description { get; set; }
			public string type { get; set; }
			public int display_order { get; set; }
			public bool system { get; set; }
			public string label { get; set; }
			public int color1 { get; set; }
			public bool visible { get; set; }
			public int color3 { get; set; }
			public int color2 { get; set; }

			[JsonConverter(typeof(JSON.StreamDataConverter))]
			public List<Data> data { get; set; }
			public string physical_quantity { get; set; }
			public int order { get; set; }
			public string unit { get; set; }
			public string name { get; set; }

		public Stream ()
		{
		}

		public StreamDataType DataType
		{
			get {
				if (this.type == "R")
					return StreamDataType.Raw;

				if (this.name.StartsWith("power-price-"))
					return StreamDataType.Price;

				if (this.name.Contains ("power-"))
					return StreamDataType.PowerPerHour;
			
				return StreamDataType.Raw;
			}
		}

		public enum StreamDataType
		{
			Raw,
			Power,
			PowerPerHour,
			Price
		}

	}

	public class StreamDataConverter : JsonConverter
	{

		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException ();
		}

		public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			Newtonsoft.Json.Linq.JArray array = serializer.Deserialize<Newtonsoft.Json.Linq.JArray>(reader);
		

			List<Stream.Data> data = new List<Stream.Data> ();
			foreach (var item in array) {
				Stream.Data d = new Stream.Data ();
				double dateVal = item.First.ToObject<double> ();
				DateTime dat= DateTime.MinValue.AddYears(1969).AddMilliseconds (dateVal * 1000d);

				d.date = dat;

				string valueVal = item.Last.ToString();
				double value = 0;
				double.TryParse (valueVal, out value);
				
				d.value = value;
				//d.value = .ToObject<double> ();
				data.Add (d);
			}
		
			return data;
		}

		public override bool CanConvert (Type objectType)
		{
			return true;
		}





	}

}

