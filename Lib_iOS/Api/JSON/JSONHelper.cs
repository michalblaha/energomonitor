﻿using System;
using Newtonsoft.Json;


namespace Energomonitor.Lib.iOS.Api.JSON
{
	public static class JSONHelper
	{

		static string dailyTemplate = "https://app.energomonitor.cz/d/v0/local-day/<FEED-ID>/<YEAR>-<MONTH>-<DAY>.json" ;
		static string weeklyTemplate = "https://app.energomonitor.cz/d/v0/local-week-15min/<FEED-ID>/<YEAR>-<MONTH>-<DAY>.json" ;
		static string monthlyTemplate = "https://app.energomonitor.cz/d/v0/month-agg/<FEED-ID>/<YEAR>-<MONTH>.json" ;


		static JsonSerializerSettings deserialization_settings = new JsonSerializerSettings () {
				MissingMemberHandling = MissingMemberHandling.Ignore,
				DefaultValueHandling= DefaultValueHandling.Populate,
			};


		public static string GetDailyURL(string feedId) {
			return GetDailyURL (feedId, DateTime.UtcNow.Date);
		}

		public static string GetDailyURL(string feedId, DateTime date)
		{
			return formatTemplate (dailyTemplate, feedId, date);
		}
		public static string GetMonthlyURL(string feedId, DateTime date)
		{
			return formatTemplate (monthlyTemplate, feedId, date);
		}
		public static string GetWeeklyURL(string feedId, DateTime date)
		{
			return formatTemplate (weeklyTemplate, feedId, date);
		}

		public static Config ParseConfig(string json) {
			return Newtonsoft.Json.JsonConvert.DeserializeObject<Config> (json, deserialization_settings);
		}

		private static string formatTemplate(string template,string feedId, DateTime date)
		{
			return template
				.Replace("<FEED-ID>",feedId)
				.Replace("<YEAR>",date.ToString("yyyy"))
				.Replace("<MONTH>",date.ToString("MM"))
				.Replace("<DAY>",date.ToString("dd"));
		}



	}

}