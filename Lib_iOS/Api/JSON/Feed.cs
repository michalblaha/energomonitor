﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Energomonitor.Lib.iOS.Api.JSON
{
	public class Feed
	{
		public class Difference
		{
			public DateTime PrevDay { get; private set; }
			public DateTime CurrDay { get; private set; }
			public double PrevDayValue { get; private set; }
			public double CurrDayValue { get; private set; }
			public double PrevDayTotalSum { get; private set; }

			public Difference(DateTime prevDay, DateTime currDay, double prevDaySum, double currDaySum, double prevDayTotalSum)
			{
				this.PrevDay = prevDay;
				this.CurrDay = currDay;
				this.PrevDayValue = prevDaySum;
				this.CurrDayValue = currDaySum;
				this.PrevDayTotalSum = prevDayTotalSum;
			}

			public double DiffInPercent
			{
				get { 
					if (PrevDayValue == 0)
						return 0;

					// (12-10)/10 = 0.2
					// (8-10)/10 = -0.2
					return ((CurrDayValue - PrevDayValue)/PrevDayValue ) * 100;
				}

			}
			public double Diff
			{
				get { 
					return CurrDayValue - PrevDayValue;
				}
			}
		}

		public string Name { get; private set; }

		public List<Stream> StreamCurrDay { get; private set; }
		public List<Stream> StreamPrevDay { get; private set; }
		public List<Stream> StreamCurrWeek { get; private set; }
		public List<Stream> StreamPrevWeek { get; private set; }
		public List<Stream> StreamCurrMonth { get; private set; }
		public List<Stream> StreamPrevMonth { get; private set; }

		private Feed ()
		{
		}

		public Difference DailyDifference(string streamName)
		{
			//
			Stream today = StreamCurrDay.FirstOrDefault (m => m.name == streamName);
			if (today == null)
				return null;
			Stream prevday = StreamPrevDay.FirstOrDefault (m => m.name == streamName);
			if (today == null)
				return null;

			//midnight
			var lastTimeCurrDay = today.data.Last ().date;
			var lastTimePrevDay = lastTimeCurrDay.AddDays (-1);

			if (today.data.Count == 0)
				return new Difference(lastTimePrevDay, lastTimeCurrDay, 0,0,0);

			double diffTimeInHours = (prevday.data [1].date - prevday.data [0].date).TotalHours;


			double todaySum = today.data
				.Select(m=>Calculate(today.DataType,m.value,diffTimeInHours))
				.Sum(); 


			double prevDaySum = prevday.data
				.Where (m => m.date <= lastTimePrevDay)
				.Select (m => Calculate(prevday.DataType,m.value,diffTimeInHours))
				.Sum ();

			double prevDayTotalSum = prevday.data
				.Select (m => Calculate(prevday.DataType,m.value,diffTimeInHours))
				.Sum ();

			return new Difference (lastTimePrevDay.Date, lastTimeCurrDay.Date, prevDaySum, todaySum, prevDayTotalSum );
		}
			 

					private double Calculate(Stream.StreamDataType streamType, double value, double diffInHours)
		{
			switch (streamType) {
			case Stream.StreamDataType.Raw:
				return value*diffInHours/1000;
				break;

			case Stream.StreamDataType.Power:
			case Stream.StreamDataType.PowerPerHour:
			case Stream.StreamDataType.Price:
			default:
				return value;
			}
		}

		//public string get

		public async static Task<Feed> LoadFeedsFromConfig(Config config, HttpWrapper http)
		{
			Feed feed = new Feed ();
			feed.Name = config.feeds [0].id; //zatim pouzivam jen prvni feed

			//pametove narocne, nutne prepsat na JObject.Load(reader)
			DateTime currDate = DateTime.Today;
			feed.StreamCurrDay =  await ParseJsonToStream (JSONHelper.GetDailyURL (feed.Name, currDate), http);
			feed.StreamPrevDay = await ParseJsonToStream (JSONHelper.GetDailyURL (feed.Name, currDate.AddDays(-1d)), http);

			feed.StreamCurrWeek =  await ParseJsonToStream (JSONHelper.GetWeeklyURL (feed.Name, StartOfWeek(currDate,DayOfWeek.Monday)), http);
			feed.StreamPrevWeek = await ParseJsonToStream (JSONHelper.GetWeeklyURL (feed.Name, StartOfWeek(currDate,DayOfWeek.Monday).AddDays(-7d)), http);

			//feed.StreamCurrMonth = ParseJsonToStream (JSONHelper.GetMonthlyURL (feed.Name, currDate.AddDays(-(double)(currDate.Day-1))), http,"aggregates").Result;
			//feed.StreamPrevMonth = await ParseJsonToStream (JSONHelper.GetMonthlyURL (feed.Name, currDate.AddDays(-(double)(currDate.Day-1)).AddMonths(-1)), http,"aggregates");

			return feed;
		}



		private static async Task<List<Stream>> ParseJsonToStream (string jsonName, HttpWrapper http){
			Newtonsoft.Json.Linq.JObject jobj=Newtonsoft.Json.Linq.JObject.Parse( await http.GetJson(jsonName));
			var tstreams = jobj.SelectToken ("streams").Children ().Select (m => ((Newtonsoft.Json.Linq.JProperty)m).Value.ToString ());

			List<Energomonitor.Lib.iOS.Api.JSON.Stream> streams = new List<Energomonitor.Lib.iOS.Api.JSON.Stream> ();
			foreach (var tstream in tstreams) {
				Energomonitor.Lib.iOS.Api.JSON.Stream stream = 
					Newtonsoft.Json.JsonConvert.DeserializeObject<Energomonitor.Lib.iOS.Api.JSON.Stream> (tstream);
				streams.Add (stream);
			}

			return streams;

		}

		private static async Task<List<Stream>> ParseJsonToAgregates (string jsonName, HttpWrapper http){
			Newtonsoft.Json.Linq.JObject jobj=Newtonsoft.Json.Linq.JObject.Parse( await http.GetJson(jsonName));
			var tstreams = jobj.SelectToken ("aggregates").Children ().Select (m => ((Newtonsoft.Json.Linq.JProperty)m).Value.ToString ());

			List<Energomonitor.Lib.iOS.Api.JSON.Stream> streams = new List<Energomonitor.Lib.iOS.Api.JSON.Stream> ();
			foreach (var tstream in tstreams) {
				Energomonitor.Lib.iOS.Api.JSON.Stream stream = 
					Newtonsoft.Json.JsonConvert.DeserializeObject<Energomonitor.Lib.iOS.Api.JSON.Stream> (tstream);
				streams.Add (stream);
			}

			return streams;

		}

		public static DateTime StartOfWeek( DateTime dt, DayOfWeek startOfWeek)
		{
			int diff = dt.DayOfWeek - startOfWeek;
			if (diff < 0)
			{
				diff += 7;
			}

			return dt.AddDays(-1 * diff).Date;
		}
	}
}

