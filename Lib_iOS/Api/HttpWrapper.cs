﻿using System;
using System.Threading.Tasks;

namespace Energomonitor.Lib.iOS.Api
{
	public class HttpWrapper
	{
		string baseURL = "https://app.energomonitor.cz/";
		AuthCookies auth = null;

		public string Login { get; private set; }

		public string Password { get; private set; }

		public HttpWrapper (string login, string password)
		{
			this.Login = login;
			this.Password = password;

		}

		private class AuthCookies
		{
			public System.Net.Cookie sessionCookie = null;
			public System.Net.Cookie tokenCookie = null;

			public AuthCookies (System.Net.Cookie session, System.Net.Cookie token)
			{
				this.sessionCookie = session;
				this.tokenCookie = token;
			}
		}

		private AuthCookies GetAuthCookies ()
		{
			string content = "";
			System.Net.Cookie tokenCookie = null;
			System.Net.Cookie sessionCookie = null;

			NetworkIndicator.EnterActivity ();

			using (Devmasters.Core.Web.URLContent url = new Devmasters.Core.Web.URLContent (baseURL + "login?next=/")) {
				content = url.GetContent ();

				//read cookies
				//csrftoken	iuKmbwrPSwY5ugfzKbpDnvfpT29n2NOz
				//sessionid	db72da373c6ef7eb3e34bc3ce8fa8d0e
				tokenCookie = url.ResponseParams.Cookies ["csrftoken"];
				sessionCookie = url.ResponseParams.Cookies ["sessionid"];

			}
			//get security login token
			//from <input type='hidden' name='csrfmiddlewaretoken' value='iuKmbwrPSwY5ugfzKbpDnvfpT29n2NOz' />

			//string strRegex = @"name='csrfmiddlewaretoken'\s*value='(?<token>\w*)'";
			//Regex reg = new Regex (strRegex, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.CultureInvariant);
			//if (reg.Match (content).Success) {
			//	token = reg.Match (content).Groups ["token"].Value;
			//}

			using (Devmasters.Core.Web.URLContent url = new Devmasters.Core.Web.URLContent (baseURL + "login")) {
				url.Method = Devmasters.Core.Web.MethodEnum.POST;
				url.RequestParams.Form.Add ("csrfmiddlewaretoken", tokenCookie.Value);
				url.RequestParams.Form.Add ("username", this.Login);
				url.RequestParams.Form.Add ("password", this.Password);
				url.RequestParams.Form.Add ("next", "/");

				url.RequestParams.Cookies.Add (sessionCookie);
				url.RequestParams.Cookies.Add (tokenCookie);
				url.Request.AllowAutoRedirect = false;
				//send request & make login
				var tmpcontent = url.GetContent ();
				if (tmpcontent.Length > 10)
					return null;
				//this session is logged, save it
				sessionCookie = url.ResponseParams.Cookies ["sessionid"];
			}

			NetworkIndicator.LeaveActivity ();

			return new AuthCookies (sessionCookie, tokenCookie);
	
		}

		public bool IsValidLoginCredentials ()
		{
			return (GetAuthCookies () != null);
		}

		public async Task<string> GetConfig ()
		{
			//await Task.Delay (1);
			Console.WriteLine ("HttpW GetConfig start");
			NetworkIndicator.EnterActivity ();

			if (auth == null) {
				auth = GetAuthCookies ();
				if (auth == null)
					return string.Empty;
			}
			//get next data, config.json
			using (Devmasters.Core.Web.URLContent url = new Devmasters.Core.Web.URLContent (baseURL + "config.json")) {
				url.RequestParams.Cookies.Add (auth.sessionCookie);
				url.RequestParams.Cookies.Add (auth.tokenCookie);

				url.Request.Accept = "application/json, text/javascript, */*; q=0.01";
				url.Request.Referer = "https://app.energomonitor.cz/dashboard";
				url.RequestParams.Headers.Add ("DNT", "1");
				url.RequestParams.Headers.Add ("X-Requested-With", "XMLHttpRequest");
				url.RequestParams.Headers.Add ("Cache-Control", "max-age=0");

				NetworkIndicator.LeaveActivity ();
				Console.WriteLine ("HttpW GetConfig end");

				return url.GetContent ();
			}
		}

		public async Task<string> GetJson (string fullUrl)
		{
			Console.WriteLine ("HttpW Getjson start " + fullUrl);
			//await Task.Delay (1);
			NetworkIndicator.EnterActivity ();

			if (auth == null) {
				auth = GetAuthCookies ();
				if (auth == null)
					return string.Empty;
			}
			//get next data, config.json
			try {
				Devmasters.Core.Web.URLContent url = new Devmasters.Core.Web.URLContent (fullUrl); 
				url.RequestParams.Cookies.Add (auth.sessionCookie);
				url.RequestParams.Cookies.Add (auth.tokenCookie);

				url.Request.Accept = "application/json, text/javascript, */*; q=0.01";
				url.Request.Referer = "https://app.energomonitor.cz/dashboard";
				url.RequestParams.Headers.Add ("DNT", "1");
				url.RequestParams.Headers.Add ("X-Requested-With", "XMLHttpRequest");
				url.RequestParams.Headers.Add ("Cache-Control", "max-age=0");


				return url.GetContent ();

			} finally {
				NetworkIndicator.LeaveActivity ();
				Console.WriteLine ("HttpW GetConfig end " + fullUrl);

			}

		}
	}
}