﻿using System;
using MonoTouch.Foundation;
using MonoTouch.Security;
using System.IO;
using MonoTouch.UIKit;

namespace Energomonitor.Lib.iOS
{
	public abstract class KeyChainBase
	{

		protected class KeyChainReturn {
			public string Key = null;
			public string Value = null;
			public SecStatusCode Status;
		}

		public SecKind Kind { get; private set; }
		public bool SyncToCloud { get; private set; }
		public string Label { get; private set; }
		public string Service { get; private set; }


		public KeyChainBase(string service, bool syncToCloud = false, SecKind kind = SecKind.GenericPassword, string label = null)
		{
			this.Kind = kind;
			this.SyncToCloud = syncToCloud;
			this.Service = service;
			if (string.IsNullOrEmpty (label))
				label = service;

			this.Label = label;
		}
		protected virtual SecRecord GetBaseRecord()
		{
			return new SecRecord (this.Kind) {
				Service = Service,
				Label = Label,
				Synchronizable = SyncToCloud
			};
		}

		protected virtual SecStatusCode DeleteData ()
		{
			return DeleteData (null);
		}
		protected virtual SecStatusCode DeleteData (string key)
		{
			// Query and remove.
			SecRecord queryRec = GetBaseRecord ();
			if (!string.IsNullOrEmpty (key))
				queryRec.Account = key;

			SecStatusCode code = SecKeyChain.Remove (queryRec);
			return code;
		}


		protected virtual SecStatusCode SetData (string key, string value, SecAccessible secAccessible)
		{
			if (key == null) {
				throw new ArgumentNullException ("key");
			}

			if (value == null) {
				throw new ArgumentNullException ("value");
			}
				
			// Don't bother updating. Delete existing record and create a new one.
			var delStatus = DeleteData(key);

			// Create a new record.
			// Store password UTF8 encoded.
			var rec = GetBaseRecord ();
			rec.Account = key;
			rec.Accessible = secAccessible;
			rec.ValueData = NSData.FromString (value, NSStringEncoding.UTF8);

			SecStatusCode code = SecKeyChain.Add (rec);

			return code;
		}


		protected virtual KeyChainReturn GetKeyValue()
		{

			SecStatusCode code;
			// Query the record.
			SecRecord queryRec = GetBaseRecord();

			queryRec = SecKeyChain.QueryAsRecord (queryRec, out code);

			// If found, try to get password.
			if (code == SecStatusCode.Success && queryRec != null && queryRec.ValueData != null) {
				// Decode from UTF8.
				return new KeyChainReturn () {
					Status = code,
					Key = queryRec.Account,
					Value = NSString.FromData (queryRec.ValueData, NSStringEncoding.UTF8)
				};
			}
		 	else
				return new KeyChainReturn() {Status = code};
		}

	
	}
}