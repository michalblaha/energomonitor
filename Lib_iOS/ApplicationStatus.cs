﻿using System;
using System.Threading.Tasks;

namespace Energomonitor.Lib.iOS
{
	public class ApplicationStatus
	{
		public enum CredentialStatus
		{
			Valid,
			Unknown,
			Empty,
		}

		public Energomonitor.Lib.iOS.SecureLogin SecureLogin = new Energomonitor.Lib.iOS.SecureLogin (false);
		public CredentialStatus LoginStatus = CredentialStatus.Unknown;
		public Api.JSON.Config Config = null;
		public Energomonitor.Lib.iOS.Api.JSON.Feed Feed = null;

		public Api.HttpWrapper HttpW = null; 

		public void Refresh()
		{
			Init ();
		}

		public void SaveValidCredentials(string username, string password)
		{
			SecureLogin.Set(username, password);
			LoginStatus = CredentialStatus.Valid;
			HttpW = new Energomonitor.Lib.iOS.Api.HttpWrapper (SecureLogin.Get ().Username, SecureLogin.Get ().Password);

		}

		public ApplicationStatus ()
		{
			Init ();
		}

		private void Init() {


			LoginStatus = CredentialStatus.Unknown;
			SecureLogin.LoginInfo li = SecureLogin.Get ();
			if (li != null) {
				Api.HttpWrapper http = new Energomonitor.Lib.iOS.Api.HttpWrapper (li.Username, li.Password);
				try {
					if (http.IsValidLoginCredentials ())
					{
						LoginStatus = CredentialStatus.Valid;
						HttpW = new Energomonitor.Lib.iOS.Api.HttpWrapper (SecureLogin.Get ().Username, SecureLogin.Get ().Password);
					}
					else {
						LoginStatus = CredentialStatus.Empty;
						SecureLogin.Delete ();
						HttpW = null;
					}

				} catch (Exception ex) {
					LoginStatus = CredentialStatus.Unknown;
				}

			} else
				LoginStatus = CredentialStatus.Empty;
		}

		public async Task DownloadConfig()
		{
			Console.WriteLine ("started DownloadConfig");


			Task<string> taskConfig = HttpW.GetConfig ();
			string configContent = await taskConfig;

			this.Config = Api.JSON.JSONHelper.ParseConfig (configContent);
			Console.WriteLine ("done DownloadConfig");
		}

		public async Task DownloadFeed()
		{
			Console.WriteLine ("started DownloadFeed");

			this.Feed = await Energomonitor.Lib.iOS.Api.JSON.Feed.LoadFeedsFromConfig (this.Config, this.HttpW);

			Console.WriteLine ("done DownloadFeed");
		}
	}
}
