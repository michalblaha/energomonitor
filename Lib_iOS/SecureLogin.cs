﻿using System;
using MonoTouch.Foundation;
using MonoTouch.Security;
using System.IO;
using MonoTouch.UIKit;

namespace Energomonitor.Lib.iOS
{
	public class SecureLogin 
		: KeyChainBase
	{

		public class LoginInfo
		{
			public string Username;
			public string Password;
		}

		public SecureLogin(bool syncToCloud = true)
			: base("login",syncToCloud)
		{
		}

		public bool Set(string username, string password)
		{
			SecStatusCode status = base.SetData (username, password, SecAccessible.WhenUnlocked);
			return status == SecStatusCode.Success;
		}

		public bool Delete()
		{
			SecStatusCode status = 	base.DeleteData();
			return status == SecStatusCode.Success;
		}

		public LoginInfo Get() {
			KeyChainBase.KeyChainReturn kr = base.GetKeyValue ();

			if (kr.Status == SecStatusCode.Success)
				return new LoginInfo (){ Username = kr.Key, Password = kr.Value };
			else
				return null;
			
		}

		public bool IsSaved()
		{
			return this.Get() != null;

		}
	
	}
}